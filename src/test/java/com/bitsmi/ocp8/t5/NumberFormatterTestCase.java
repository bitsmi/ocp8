package com.bitsmi.ocp8.t5;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Currency;
import java.util.Locale;

import org.junit.jupiter.api.Test;

public class NumberFormatterTestCase 
{
	@Test
	public void defaultInstanceFormatTest()
	{
		Locale esESLocale = new Locale("es", "ES");
		Locale enUSLocale = new Locale("en", "US");
		Locale.setDefault(esESLocale);
		
		// Default locale (es_ES)
		NumberFormat defaultFormatter = NumberFormat.getInstance();
		// en_US locale
		NumberFormat enUSFormatter = NumberFormat.getInstance(enUSLocale);
		
		String strDefaultLongValue = defaultFormatter.format(100);
		String strDefaultDoubleValue = defaultFormatter.format(100.01);
		String strDefaultExactDoubleValue = defaultFormatter.format(100D);
		String strEnUSLongValue = enUSFormatter.format(100);
		String strEnUSDoubleValue = enUSFormatter.format(100.01);
		String strEnUSExactDoubleValue = enUSFormatter.format(100D);
		
		assertThat(strDefaultLongValue).isEqualTo("100");
		// es_ES locale uses "," as decimal separator
		assertThat(strDefaultDoubleValue).isEqualTo("100,01");
		// Parsed string doesn't contains ",00" if the double value doesn't contains decimal digits   
		assertThat(strDefaultExactDoubleValue).isEqualTo("100");
		assertThat(strEnUSLongValue).isEqualTo("100");
		// en_US locale uses "." as decimal separator
		assertThat(strEnUSDoubleValue).isEqualTo("100.01");
		// Parsed string doesn't contains ".00" if the double value doesn't contains decimal digits
		assertThat(strEnUSExactDoubleValue).isEqualTo("100");
	}
	
	@Test
	public void defaultInstanceParseTest() throws ParseException
	{
		Locale esESLocale = new Locale("es", "ES");
		Locale enUSLocale = new Locale("en", "US");
		Locale.setDefault(esESLocale);
		
		// Default locale (es_ES)
		NumberFormat defaultFormatter = NumberFormat.getInstance();
		// en_US locale
		NumberFormat enUSFormatter = NumberFormat.getInstance(enUSLocale);
		
		Number defaultLongValue = defaultFormatter.parse("100");
		Number defaultDoubleValue = defaultFormatter.parse("100,01");
		Number enUSLongValue = enUSFormatter.parse("100");
		Number enUSDoubleValue = enUSFormatter.parse("100.01");
		
		// Formatter will parse until found a non parseable value
		Number partialValue = defaultFormatter.parse("100x25");
		// If the first character is a non parseable value, it will throw an exception
		Throwable nonParseableValueThrowable = catchThrowable(() -> {
			Number errorValue = defaultFormatter.parse("x25");
		});
		
		assertThat(defaultLongValue).isEqualTo(100L);
		assertThat(defaultDoubleValue).isEqualTo(100.01D);
		
		assertThat(enUSLongValue).isEqualTo(100L);
		assertThat(enUSDoubleValue).isEqualTo(100.01D);

		assertThat(partialValue).isEqualTo(100L);
		assertThat(nonParseableValueThrowable).isNotNull().isInstanceOf(ParseException.class);
	}
	
	@Test
	public void currencyInstanceFormatTest()
	{
		Locale esESLocale = new Locale("es", "ES");
		Locale enUSLocale = new Locale("en", "US");
		Locale.setDefault(esESLocale);
		
		// Default locale (es_ES)
		NumberFormat defaultFormatter = NumberFormat.getCurrencyInstance();
		// en_US locale
		NumberFormat enUSFormatter = NumberFormat.getCurrencyInstance(enUSLocale);
		
		String strDefaultLongValue = defaultFormatter.format(100);
		String strDefaultDoubleValue = defaultFormatter.format(100.01);
		String strDefaultExactDoubleValue = defaultFormatter.format(100D);
		String strEnUSLongValue = enUSFormatter.format(100);
		String strEnUSDoubleValue = enUSFormatter.format(100.01);
		String strEnUSExactDoubleValue = enUSFormatter.format(100D);
		
		/* es_ES locale uses "," as decimal separator and adds " €" at the end.
		 * All parsed values have 2 decimal digits
		 */ 
		assertThat(strDefaultLongValue).isEqualTo("100,00 €");
		assertThat(strDefaultDoubleValue).isEqualTo("100,01 €");
		assertThat(strDefaultExactDoubleValue).isEqualTo("100,00 €");
		
		/* en_US locale uses "." as decimal separator and adds "$" at the beginning.
		 * All parsed values have 2 decimal digits
		 */
		assertThat(strEnUSLongValue).isEqualTo("$100.00");
		assertThat(strEnUSDoubleValue).isEqualTo("$100.01");
		assertThat(strEnUSExactDoubleValue).isEqualTo("$100.00");
	}
	
	@Test
	public void currencyInstanceParseTest() throws ParseException
	{
		Locale esESLocale = new Locale("es", "ES");
		Locale enUSLocale = new Locale("en", "US");
		Locale.setDefault(esESLocale);
		
		// Default locale (es_ES)
		NumberFormat defaultFormatter = NumberFormat.getCurrencyInstance();
		// en_US locale
		NumberFormat enUSFormatter = NumberFormat.getCurrencyInstance(enUSLocale);
		
		Number defaultLongValue = defaultFormatter.parse("100,00 €");
		Number defaultDoubleValue = defaultFormatter.parse("100,01 €");
		Number enUSLongValue = enUSFormatter.parse("$100");
		Number enUSDoubleValue = enUSFormatter.parse("$100.01");
		
		/* Currency symbol must be in the correct position according to locale
		 * Blank spaces between digits and currency symbols must also match
		 */
		Throwable wrongDefaultSymbolPositionThrowable = catchThrowable(() -> {
			defaultFormatter.parse("100,01€");
		});
		Throwable wrongEnUSSymbolPositionThrowable = catchThrowable(() -> {
			enUSFormatter.parse("100.01$");
		});
		
		// Formatter will parse until found a non parseable value
		Number partialValue = defaultFormatter.parse("100 €x25");
		// If the first character is a non parseable value, it will throw an exception
		Throwable nonParseableValueThrowable = catchThrowable(() -> {
			Number errorValue = defaultFormatter.parse("x25");
		});

		assertThat(defaultLongValue).isEqualTo(100L);
		assertThat(defaultDoubleValue).isEqualTo(100.01D);
		
		assertThat(enUSLongValue).isEqualTo(100L);
		assertThat(enUSDoubleValue).isEqualTo(100.01D);

		assertThat(nonParseableValueThrowable).isNotNull().isInstanceOf(ParseException.class);
		assertThat(wrongDefaultSymbolPositionThrowable).isNotNull().isInstanceOf(ParseException.class);
		assertThat(wrongEnUSSymbolPositionThrowable).isNotNull().isInstanceOf(ParseException.class);
		
		assertThat(partialValue).isEqualTo(100L);
		assertThat(nonParseableValueThrowable).isNotNull().isInstanceOf(ParseException.class);
	}
	
	@Test
	public void percentInstanceFormatTest()
	{
		Locale esESLocale = new Locale("es", "ES");
		Locale enUSLocale = new Locale("en", "US");
		Locale.setDefault(esESLocale);
		
		// Default locale (es_ES)
		NumberFormat defaultFormatter = NumberFormat.getPercentInstance();
		// en_US locale
		NumberFormat enUSFormatter = NumberFormat.getPercentInstance(enUSLocale);
		
		String strDefaultLongValue = defaultFormatter.format(0.1);
		String strDefaultDoubleValue = defaultFormatter.format(0.1002);
		String strEnUSLongValue = enUSFormatter.format(0.1);
		String strEnUSDoubleValue = enUSFormatter.format(0.1002);
		
		assertThat(strDefaultLongValue).isEqualTo("10%");
		// By default, formatted percent values doesn't have decimal digits  
		assertThat(strDefaultDoubleValue).isEqualTo("10%");
		
		assertThat(strEnUSLongValue).isEqualTo("10%");
		// By default, formatted percent values doesn't have decimal digits
		assertThat(strEnUSDoubleValue).isEqualTo("10%");
	}
	
	@Test
	public void percentInstanceParseTest() throws ParseException
	{
		Locale esESLocale = new Locale("es", "ES");
		Locale enUSLocale = new Locale("en", "US");
		Locale.setDefault(esESLocale);
		
		// Default locale (es_ES)
		NumberFormat defaultFormatter = NumberFormat.getPercentInstance();
		// en_US locale
		NumberFormat enUSFormatter = NumberFormat.getPercentInstance(enUSLocale);
		
		Number defaultLongValue = defaultFormatter.parse("10%");
		Number defaultDoubleValue = defaultFormatter.parse("10,02%");
		Number enUSLongValue = enUSFormatter.parse("10%");
		Number enUSDoubleValue = enUSFormatter.parse("10.02%");
		
		/* Currency symbol must be in the correct position according to locale
		 * Blank spaces between digits and currency symbols must also match
		 */
		Throwable wrongDefaultSymbolPositionThrowable = catchThrowable(() -> {
			defaultFormatter.parse("10,02 %");
		});
		Throwable wrongEnUSSymbolPositionThrowable = catchThrowable(() -> {
			enUSFormatter.parse("10.02 %");
		});
		
		// Formatter will parse until found a non parseable value
		Number partialValue = defaultFormatter.parse("10,02%x25");
		// If the first character is a non parseable value, it will throw an exception
		Throwable nonParseableValueThrowable = catchThrowable(() -> {
			Number errorValue = defaultFormatter.parse("x25");
		});
		
		assertThat(defaultLongValue).isEqualTo(0.1D);
		assertThat(defaultDoubleValue).isEqualTo(0.1002D);
		
		assertThat(enUSLongValue).isEqualTo(0.1D);
		assertThat(enUSDoubleValue).isEqualTo(0.1002D);

		assertThat(nonParseableValueThrowable).isNotNull().isInstanceOf(ParseException.class);
		assertThat(wrongDefaultSymbolPositionThrowable).isNotNull().isInstanceOf(ParseException.class);
		assertThat(wrongEnUSSymbolPositionThrowable).isNotNull().isInstanceOf(ParseException.class);
		
		assertThat(partialValue).isEqualTo(0.1002D);
		assertThat(nonParseableValueThrowable).isNotNull().isInstanceOf(ParseException.class);
	}
	
	@Test
	public void customFormatTest()
	{
		Locale esESLocale = new Locale("es", "ES");
		Locale.setDefault(esESLocale);
		
		// Default locale (es_ES)
		NumberFormat defaultFormatter = NumberFormat.getCurrencyInstance();
		NumberFormat customFormatter = NumberFormat.getCurrencyInstance();
		customFormatter.setMinimumIntegerDigits(4);
		// Discard thousands separator "."
		customFormatter.setGroupingUsed(false);
		customFormatter.setMaximumFractionDigits(1);
		customFormatter.setMinimumFractionDigits(1);
		customFormatter.setCurrency(Currency.getInstance("JPY"));
		// Separator symbols and currency position cannot be customized
		
		/* DEFAULT */
		String strDefaultLongValue = defaultFormatter.format(100);
		String strDefaultDoubleValue = defaultFormatter.format(100.01);
		String strDefaultExactDoubleValue = defaultFormatter.format(100D);
		/* CUSTOM */
		String strCustomLongValue = customFormatter.format(100);
		String strCustomDoubleValue = customFormatter.format(100.01);
		String strCustomExactDoubleValue = customFormatter.format(100D);
		
		/* es_ES locale uses "," as decimal separator and adds " €" at the end.
		 * All parsed values have 2 decimal digits
		 */ 
		assertThat(strDefaultLongValue).isEqualTo("100,00 €");
		assertThat(strDefaultDoubleValue).isEqualTo("100,01 €");
		assertThat(strDefaultExactDoubleValue).isEqualTo("100,00 €");
		
		assertThat(strCustomLongValue).isEqualTo("0100,0 JPY");
		assertThat(strCustomDoubleValue).isEqualTo("0100,0 JPY");
		assertThat(strCustomExactDoubleValue).isEqualTo("0100,0 JPY");
	}
	
	@Test
	public void decimalFormatTest()
	{
		/* Pattern doesn't has to be localized
		 * DecimalFormatSymbols are used to represent the result
		 * 2 mandatory digits at int part of the number
		 * Optional 3rd+ digit(s) at int part of the number
		 * 1 mandatory number at decimal part of the number
		 * Optional 3rd digit at decimal part of the number. Unlike int part, formatter will truncate additional digits 
		 */
		NumberFormat esESFormatter = new DecimalFormat("#00.0#", DecimalFormatSymbols.getInstance(new Locale("es", "ES")));
		NumberFormat enUSFormatter = new DecimalFormat("#00.0#", DecimalFormatSymbols.getInstance(new Locale("en", "US")));
		
		String esESi1d1 = esESFormatter.format(1.5);
		String esESi2d0 = esESFormatter.format(10);
		String esESi2d1 = esESFormatter.format(10.5);
		String esESi2d3 = esESFormatter.format(10.523);
		String esESi3d1 = esESFormatter.format(100.5);
		String esESi4d1 = esESFormatter.format(1001.5);
		
		String enUSi1d1 = enUSFormatter.format(1.5);
		String enUSi2d0 = enUSFormatter.format(10);
		String enUSi2d1 = enUSFormatter.format(10.5);
		String enUSi2d3 = enUSFormatter.format(10.523);
		String enUSi3d1 = enUSFormatter.format(100.5);
		String enUSi4d1 = enUSFormatter.format(1001.5);
		
		assertThat(esESi1d1).isEqualTo("01,5");
		assertThat(esESi2d0).isEqualTo("10,0");
		assertThat(esESi2d1).isEqualTo("10,5");
		assertThat(esESi2d3).isEqualTo("10,52");
		assertThat(esESi3d1).isEqualTo("100,5");
		assertThat(esESi4d1).isEqualTo("1001,5");
		
		assertThat(enUSi1d1).isEqualTo("01.5");
		assertThat(enUSi2d0).isEqualTo("10.0");
		assertThat(enUSi2d1).isEqualTo("10.5");
		assertThat(enUSi2d3).isEqualTo("10.52");
		assertThat(enUSi3d1).isEqualTo("100.5");
		assertThat(enUSi4d1).isEqualTo("1001.5");
	}
}
