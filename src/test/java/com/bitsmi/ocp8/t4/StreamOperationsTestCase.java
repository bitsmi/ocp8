package com.bitsmi.ocp8.t4;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

public class StreamOperationsTestCase 
{
	@Test
	public void distinctSequentialTest()
	{
		StringBuilder processBuilder = new StringBuilder();
		List<String> result = Stream.of("1", "2", "1", "4", "3")
				.peek(elem -> processBuilder.append("V" + elem + ";"))
				.distinct()
				.peek(elem -> processBuilder.append("R" + elem + ";"))
				.collect(Collectors.toList());
		
		assertThat(result).containsExactly("1", "2", "4", "3");
		assertThat(processBuilder.toString()).isEqualTo("V1;R1;V2;R2;V1;V4;R4;V3;R3;");
	}
	
	@Test
	public void distinctParallelTest()
	{
		StringBuffer processBuilder = new StringBuffer();
		List<String> result = Stream.of("1", "2", "1", "4", "3")
				.parallel()
				.peek(elem -> processBuilder.append("V" + elem + ";"))
				.distinct()
				.peek(elem -> processBuilder.append("R" + elem + ";"))
				.collect(Collectors.toList());
		
		assertThat(result).containsExactly("1", "2", "4", "3");
		// E.G. V1;V3;V4;V2;V1;R4;R2;R3;R1;
		System.out.println("DISTINCT PARALLEL TEST: " + processBuilder.toString());
	}
	
	@Test
	public void sortedSequentialTest()
	{
		StringBuilder processBuilder = new StringBuilder();
		List<String> result = Stream.of("1", "2", "1", "4", "3")
				.peek(elem -> processBuilder.append("V" + elem + ";"))
				.sorted()
				.peek(elem -> processBuilder.append("R" + elem + ";"))
				.collect(Collectors.toList());
		
		assertThat(result).containsExactly("1", "1", "2", "3", "4");
		assertThat(processBuilder.toString()).isEqualTo("V1;V2;V1;V4;V3;R1;R1;R2;R3;R4;");
	}
	
	@Test
	public void sortedParallelTest()
	{
		StringBuffer processBuilder = new StringBuffer();
		List<String> result = Stream.of("1", "2", "1", "4", "3")
				.parallel()
				.peek(elem -> processBuilder.append("V" + elem + ";"))
				.sorted()
				// At this point it's still parallel, so can't determine the order of "peek" processing
				.peek(elem -> processBuilder.append("R" + elem + ";"))
				// Elements are merged and sorted into the collection by the collect() method
				.collect(Collectors.toList());
		
		assertThat(result).containsExactly("1", "1", "2", "3", "4");
		// V2;V1;V1;V4;V3;R1;R1;R2;R4;R3;
		System.out.println("SORTED PARALLEL TEST: " + processBuilder.toString());
	}	
}
