package com.bitsmi.ocp8.t7;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

public class ForkJoinTestCase 
{
	@Test
	public void recursiveTaskTest()
	{
		Integer[] values = new Integer[] {1, 2, 3, 4};
		SumTestTask task = new SumTestTask(values);
		ForkJoinPool pool = new ForkJoinPool();
		Integer sum = pool.invoke(task);
		
		assertThat(sum).isEqualTo(10);
	}
	
	@Test
	public void recursiveActionTest()
	{
		Integer[] values = new Integer[] {1, 2, 3, 4};
		SumTestAction action = new SumTestAction(values);
		ForkJoinPool pool = new ForkJoinPool();
		pool.invoke(action);
		
		assertThat(action.getResult()).isEqualTo(10);
	}
	
	private static class SumTestTask extends RecursiveTask<Integer>
	{
		private static final long serialVersionUID = 1L;
		
		private Integer[] values;
		
		public SumTestTask(Integer[] values) 
		{
			this.values = values;
		}
		
		@Override
		protected Integer compute() 
		{
			if(values.length==0) {
				return 0;
			}
			else if(values.length==1) {
				return values[0];
			}
			
			int middle = values.length/2;
			Integer[] subtaskValues1 = Stream.of(values)
					.limit(middle)
					.collect(Collectors.toList())
					.toArray(new Integer[] {});
			Integer[] subtaskValues2 = Stream.of(values)
					.skip(middle)
					.collect(Collectors.toList())
					.toArray(new Integer[] {});
			
			SumTestTask subtask1 = new SumTestTask(subtaskValues1);
			SumTestTask subtask2 = new SumTestTask(subtaskValues2);
			
			subtask1.fork();
			subtask2.fork();

			return subtask1.join() + subtask2.join();
		}
	}
	
	private static class SumTestAction extends RecursiveAction
	{
		private static final long serialVersionUID = 1L;

		private Integer[] values;
		private Integer result;
		
		public SumTestAction(Integer[] values) 
		{
			this.values = values;
		}
		
		@Override
		protected void compute() 
		{
			if(values.length==0) {
				result = 0;
				return;
			}
			else if(values.length==1) {
				result = values[0];
				return;
			}
			
			int middle = values.length/2;
			Integer[] subtaskValues1 = Stream.of(values)
					.limit(middle)
					.collect(Collectors.toList())
					.toArray(new Integer[] {});
			Integer[] subtaskValues2 = Stream.of(values)
					.skip(middle)
					.collect(Collectors.toList())
					.toArray(new Integer[] {});
			
			SumTestAction subaction1 = new SumTestAction(subtaskValues1);
			SumTestAction subaction2 = new SumTestAction(subtaskValues2);

			// invoke and wait
			invokeAll(subaction1, subaction2);
			// Or if only 1 subaction --> subaction.invoke();
			
			result = subaction1.getResult() + subaction2.getResult();
		}

		public Integer getResult() 
		{
			return result;
		}
	}
}
