package com.bitsmi.ocp8.t9;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

public class FilesTestCase 
{
	@Test
	public void walkStreamTest() throws IOException
	{
		List<String> results = Files.walk(Paths.get("src"), FileVisitOption.FOLLOW_LINKS)
				.map(p -> p.getFileName().toString())
				.filter(name -> name.endsWith(".java"))
				.collect(Collectors.toList());
		
		assertThat(results).isNotEmpty()
				.contains("FilesTestCase.java");
	}
	
	@Test
	public void findTest() throws IOException
	{
		List<String> results = Files.find(Paths.get("src"), 
						10, 
						(path, attr) -> path.getFileName().toString().endsWith(".java")
						//, Optional FileVisitorOption to follow links
						)
				.map(p -> p.getFileName().toString())
				.collect(Collectors.toList());
		
		assertThat(results).isNotEmpty()
				.allMatch(filename -> filename.endsWith(".java"));
	}
	
	@Test
	public void visitorFindTest() throws IOException
	{
		class TestFileVisitor extends SimpleFileVisitor<Path> 
		{
			Path searchResult = null;
			
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException 
			{
				if("FilesTestCase.java".equals(file.getFileName().toString())) {
					searchResult = file;
					return FileVisitResult.TERMINATE;
				}
				return FileVisitResult.CONTINUE;
			}
		};
		
		TestFileVisitor visitor = new TestFileVisitor();
		
		Files.walkFileTree(Paths.get("src"), visitor);
		
		assertThat(visitor.searchResult).isNotNull()
				.extracting(p -> p.getFileName().toString())
				.containsOnly("FilesTestCase.java");
	}
}
