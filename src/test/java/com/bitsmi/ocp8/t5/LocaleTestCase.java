package com.bitsmi.ocp8.t5;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.Locale;
import org.junit.jupiter.api.Test;

public class LocaleTestCase 
{
	@Test
	public void LocaleBuilderTest()
	{
		Locale.Builder builder = new Locale.Builder()
				.setLanguage("en")
				.setRegion("US");
		
		Locale builtLocale = builder.build();
		Locale referenceLocale = new Locale("en", "US");
		
		assertThat(builtLocale).isEqualTo(referenceLocale);
	}
}
