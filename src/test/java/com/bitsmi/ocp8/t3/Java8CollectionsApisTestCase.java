package com.bitsmi.ocp8.t3;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

public class Java8CollectionsApisTestCase 
{
	/*---------------------------------*
	 * COLLECTION NEW APIs
	 *---------------------------------*/
	@Test
	public void removeIfTest()
	{
		// Arrays.asList is unmodificable. It can't be used in this test
		List<String> testList = Stream.of("aaa", "aab", "bbb", "ccc")
				.collect(Collectors.toList());
		
		boolean elementsRemoved = testList.removeIf(e -> e.startsWith("a"));
		
		assertThat(elementsRemoved).isTrue();
		assertThat(testList).hasSize(2);
		assertThat(testList).containsOnly("bbb", "ccc");
	}
	
	@Test
	public void replaceAllTest()
	{
		List<String> testList = Arrays.asList("aaa", "aab", "bbb", "ccc");
		
		testList.replaceAll(e -> e + "_MOD");
		
		assertThat(testList).hasSize(4);
		assertThat(testList).containsOnly("aaa_MOD", "aab_MOD", "bbb_MOD", "ccc_MOD");
	}
	
	@Test
	public void forEachTest()
	{
		List<String> testList = Arrays.asList("aaa", "aab", "bbb", "ccc");
		
		AtomicInteger count = new AtomicInteger(0);
		testList.forEach(e -> count.incrementAndGet());
		
		assertThat(count.get()).isEqualTo(4);
	}
	
	/*---------------------------------*
	 * MAP NEW APIs
	 *---------------------------------*/
	@Test
	public void putIfAbsentTest()
	{
		Map<String, Integer> testMap = new HashMap<>();
		testMap.put("key1", 1);
		testMap.put("key2", 2);
		testMap.put("key3", 3);
		testMap.put("keyNull", null);
		
		testMap.putIfAbsent("key4", 4);
		testMap.putIfAbsent("key1", 10);
		// null mapped values are managed as absent
		testMap.putIfAbsent("keyNull", 99);
		
		assertThat(testMap).hasSize(5);
		assertThat(testMap).containsOnly(entry("key1", 1), entry("key2", 2), entry("key3", 3), entry("key4", 4), entry("keyNull", 99));
	}
	
	@Test
	public void mergeTest()
	{
		Map<String, Integer> testMap = new HashMap<>();
		testMap.put("key1", 1);
		testMap.put("key2", 2);
		testMap.put("key3", 3);
		// 
		testMap.put("keyNull", null);
		
		// BiFunction<param1Type, param2Type, returnType>
		BiFunction<Integer, Integer, Integer> merger = (oldValue, newValue) -> {
			return oldValue + newValue;
		};
		
		testMap.merge("key4", 4, merger);
		testMap.merge("key1", 10, merger);
		// null mapped values are managed as absent
		testMap.merge("keyNull", 99, merger);
		
		assertThat(testMap).hasSize(5);
		assertThat(testMap).containsOnly(entry("key1", 11), entry("key2", 2), entry("key3", 3), entry("key4", 4), entry("keyNull", 99));
	}
	
	@Test
	public void computeIfPresentTest()
	{
		Map<String, Integer> testMap = new HashMap<>();
		testMap.put("key1", 1);
		testMap.put("key2", 2);
		testMap.put("key3", 3);
		testMap.put("keyNull", null);
		
		// BiFunction<param1Type, param2Type, returnType>
		BiFunction<String, Integer, Integer> function = (k, v) -> {
			if("key1".equals(k)) {
				return 100;
			}
			else if("key2".equals(k)) {
				return null;
			}
			
			return 0;  
		};
		
		Integer computedValue1 = testMap.computeIfPresent("key1", function);
		// null computed values removes the key from the map
		Integer computedValue2 = testMap.computeIfPresent("key2", function);
		// null mapped values are managed as absent
		Integer computedValueNull = testMap.computeIfPresent("keyNull", function);
		Integer computedValue99 = testMap.computeIfPresent("key99", function);
		
		assertThat(computedValue1).isEqualTo(100);
		assertThat(computedValue2).isNull();
		assertThat(computedValueNull).isNull();
		assertThat(computedValue99).isNull();
		
		// null computed values removes the key from the map
		assertThat(testMap).doesNotContainKey("key2");
		// Computed values are stored in the map
		assertThat(testMap).containsOnly(entry("key1", 100), entry("key3", 3), entry("keyNull", null));
	}
	
	@Test
	public void computeIfAbsentTest()
	{
		Map<String, Integer> testMap = new HashMap<>();
		testMap.put("key1", 1);
		testMap.put("key2", 2);
		testMap.put("key3", 3);
		testMap.put("keyNull", null);
		
		Function<String, Integer> function = (k) -> {
			return "key99".equals(k) ? 99 : 0;
		};
		
		// For already mapped values, returns the current value
		Integer computedValue1 = testMap.computeIfAbsent("key1", function);
		// For non mapped values, compute, add to the map and return it
		Integer computedValue99 = testMap.computeIfAbsent("key99", function);
		// For null mapped values, compute, add to the map and return it
		Integer computedValueNull = testMap.computeIfAbsent("keyNull", function);
		
		assertThat(computedValue1).isEqualTo(1);
		assertThat(computedValue99).isEqualTo(99);
		assertThat(computedValueNull).isEqualTo(0);
		// Computed values are stored in the map
		assertThat(testMap).contains(entry("key1", 1), entry("keyNull", 0), entry("key99", 99));
	}
}
