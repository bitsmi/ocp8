package com.bitsmi.ocp8.t3;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.function.Function;

import org.junit.jupiter.api.Test;

public class GenericsWildcardTestCase 
{
	@Test
	public void upperBoundsTest()
	{
		DummyValue<Exception> eDummy = new DummyValue<>(new Exception());
		DummyValue<IOException> ioDummy = new DummyValue<>(new IOException());
		DummyValue<FileNotFoundException> fnfDummy = new DummyValue<>(new FileNotFoundException());
		
		DummyValue<? extends IOException> upperBoundDummy = null;
		
		// DOESN'T COMPILE: Generic type <Exception> doesn't extends IOException
//		upperBoundDummy = eDummy; 	
		
		// OK: Generic types <IOException> is compatible with extends IOException
		upperBoundDummy = ioDummy;
		
		// OK: Generic types <FileNotFoundException> is compatible with extends IOException
		upperBoundDummy = fnfDummy;
		
		// OK: Return type is IOException or a subclass, so it's compatible with Exception
		Exception eUpperBound = upperBoundDummy.getValue();
		
		// OK: Return type is IOException or a subclass, so it's compatible with IOException
		IOException ioEUpperBound = upperBoundDummy.getValue();
		
		// DOESN'T COMPILE: Object doesn't extends IOException
//		upperBoundDummy.setValue(new Object());
		
		// DOESN'T COMPILE: Exception doesn't extends IOException
//		upperBoundDummy.setValue(new Exception());
		
		/* DOESN'T COMPILE: Objects with upper bound wildcard or unbounded are immutables.
		 * Java cannot guarantee the real type of the object at runtime (DummyValue<IOException>, DummyValue<FileNotFoundException>...)
		 * Lower bound needed!!
		 */												 
//		upperBoundDummy.setValue(new IOException());
//		upperBoundDummy.setValue(new FileNotFoundException());
//		Object objParam = new IOException();
//		upperBoundDummy.setValue(objParam);
	}
	
	@Test
	public void lowerBoundsTest()
	{
		DummyValue<Exception> eDummy = new DummyValue<>(new Exception());
		DummyValue<IOException> ioDummy = new DummyValue<>(new IOException());
		DummyValue<FileNotFoundException> fnfDummy = new DummyValue<>(new FileNotFoundException());
		
		DummyValue<? super IOException> lowerBoundDummy = null;
		
		// OK: DummyValue instances have compatible generic types that are super class of IOException
		lowerBoundDummy = eDummy;
		lowerBoundDummy = ioDummy;
		// DOESN'T COMPILE: FileNotFoundException generic type is not a super class of IOException
//		lowerBoundDummy = fnfDummy;
		/* DOESN'T COMPILE: Java cannot guarantee that the return type fits the specified type because 
		 * it can be of IOException's super type, in this example, and they may be incompatible.
		 * In that case, always return Object
		 */
//		Exception eLowerBound = lowerBoundDummy.getValue();
//		IOException ioELowerBound = lowerBoundDummy.getValue();
		Object objLowerBound = lowerBoundDummy.getValue();
		// DOESN'T COMPILE: Exception is not compatible with IOException 
//		lowerBoundDummy.setValue(new Exception());
		/* Lower bounds enforce that the specified parameter 
		 * is of the same type or a subclass of the generic type
		 */
		lowerBoundDummy.setValue(new IOException());
		lowerBoundDummy.setValue(new FileNotFoundException());
	}
	
	@Test
	public void funtionalWildcardBoundsTest()
	{
		Function<Exception, IOException> eF = e -> new IOException(e);
		Function<Exception, Exception> eFR = e -> new IOException(e);
		Function<IOException, IOException> ioF = e -> new IOException(e);
		Function<FileNotFoundException, IOException> fnfF = e -> new IOException(e);

		/* Same definition of lambdas used in JSE8 methods
		 * lower bounds on "input" and upper on "output"
		 */
		Function<? super IOException, ? extends IOException> fLowerUpperReference = null;
		fLowerUpperReference = eF;
		// DOESN'T COMPILE: eFR function's output (Exception) is not compatible with fLowerUpperReference output (IOException) 
//		fLowerUpperReference = eFR;
		// Compile because <IOException, IOException> is compatible with <? super IOException, ? extends IOException>
		fLowerUpperReference = ioF;
		// DOESN'T COMPILE: eFR function's output (Exception) is not compatible with fLowerUpperReference output (IOException)
//		fLowerUpperReference = fnfF;
		
		/* DOESN'T COMPILE: Lower bounds enforce that the specified parameter 
		 * is of the same type or a subclass of the generic type
		 */
//		fLowerUpperReference.apply(new Exception());
		fLowerUpperReference.apply(new IOException());
		// Compiles because FileNotFoundexception can be cast to IOException
		fLowerUpperReference.apply(new FileNotFoundException());
		
		Function<? extends IOException, ? extends IOException> fUpperUpperReference = null;
		// DOESN'T COMPILE: Exception is not compatible with function's "? extends IOException"
//		fUpperUpperReference = eF;
//		fUpperUpperReference = eFR;
		// Compile becase IOException and FileNotFoundException are compatible with function's "? extends IOException" 
		fUpperUpperReference = ioF;
		fUpperUpperReference = fnfF;

		// DOESN'T COMPILE: Exception is not compatible with function's input declaration
//		fUpperUpperReference.apply(new Exception());
		/* DOESN'T COMPILE: Objects with upper bound wildcard or unbounded are immutables.
		 * Java cannot guarantee the real type of the function's input object at runtime
		 * Lower bound needed!!
		 */	
//		fUpperUpperReference.apply(new IOException());
//		fUpperUpperReference.apply(new FileNotFoundException());
	}
	
	/*------------------------------*
	 * SUPPORT METHODS AND CLASSES
	 *------------------------------*/
	public static class DummyValue<T>
	{
		private T value;
		
		public DummyValue(T value)
		{
			this.value = value;
		}
		
		public T getValue()
		{
			return value;
		}
		
		public void setValue(T value)
		{
			this.value = value;
		}
	}
}
