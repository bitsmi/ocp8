package com.bitsmi.ocp8.t3;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

public class ComparatorTestCase 
{
	@RepeatedTest(5)
	public void naturalOrderTest()
	{
		List<String> testList = Arrays.asList("aaa", "aba", "aab", "bbb");
		Collections.shuffle(testList);
		
		Collections.sort(testList, Comparator.naturalOrder());
		assertThat(testList).hasSize(4);
		assertThat(testList).containsExactly("aaa", "aab", "aba", "bbb");
	}
	
	@RepeatedTest(5)
	public void reversedOrderTest()
	{
		List<String> testList = Arrays.asList("aaa", "aba", "aab", "bbb");
		Collections.shuffle(testList);
		
		Collections.sort(testList, Comparator.reverseOrder());
		assertThat(testList).hasSize(4);
		assertThat(testList).containsExactly("bbb", "aba", "aab", "aaa");
	}
	
	@Test
	public void naturalOrderNullsFailTest()
	{
		List<String> testList = Arrays.asList(null, "aaa", "aba", "aab", "bbb");
		Collections.shuffle(testList);
		
		Throwable thrown = catchThrowable(() -> {
			Collections.sort(testList, Comparator.naturalOrder());
		});
		
		assertThat(thrown).isInstanceOf(NullPointerException.class);
	}
	
	@RepeatedTest(5)
	public void naturalOrderNullsSafeTest()
	{
		List<String> testList = Arrays.asList(null, "aaa", "aba", "aab", "bbb");
		Collections.shuffle(testList);
		
		Collections.sort(testList, Comparator.nullsFirst(Comparator.naturalOrder()));
		
		assertThat(testList).hasSize(5);
		assertThat(testList).containsExactly(null, "aaa", "aab", "aba", "bbb");
	}
	
	@RepeatedTest(5)
	public void sortWithNullStringsTest()
	{
		List<TestPojo> testList = createTestListWithNullStrings();
		
		Comparator<TestPojo> comparator = Comparator
				// Field value1 can be null
				.comparing(TestPojo::getValue1, Comparator.nullsFirst(Comparator.naturalOrder()))
				.thenComparingInt(TestPojo::getValue2)
				.thenComparingLong(TestPojo::getValue3);
		Collections.sort(testList, comparator);
		
		assertThat(testList).hasSize(8);
		assertThat(testList).containsExactly(
				new TestPojo(null, 4, 400L),
				new TestPojo("test1", 1, 100L), new TestPojo("test1", 2, 100L), new TestPojo("test1", 3, 100L),
				new TestPojo("test2", 2, 100L), new TestPojo("test2", 2, 200L), new TestPojo("test2", 2, 300L),
				new TestPojo("test3", 3, 300L));
	}
	
	@RepeatedTest(5)
	public void reverseSortWithNullStringsTest()
	{
		List<TestPojo> testList = createTestListWithNullStrings();
		
		Comparator<TestPojo> comparator = Comparator
				// Field value1 can be null
				.comparing(TestPojo::getValue1, Comparator.nullsFirst(Comparator.naturalOrder()))
				.thenComparingInt(TestPojo::getValue2)
				.thenComparingLong(TestPojo::getValue3)
				.reversed();
		Collections.sort(testList, comparator);
		
		assertThat(testList).hasSize(8);
		assertThat(testList).containsExactly(
				new TestPojo("test3", 3, 300L),
				new TestPojo("test2", 2, 300L), new TestPojo("test2", 2, 200L), new TestPojo("test2", 2, 100L),  
				new TestPojo("test1", 3, 100L), new TestPojo("test1", 2, 100L), new TestPojo("test1", 1, 100L), 
				new TestPojo(null, 4, 400L));
	}
	
	@RepeatedTest(5)
	public void sortWithNullNumbersTest()
	{
		List<TestPojo> testList = createTestListWithNullNumbers();
		
		Comparator<TestPojo> comparator = Comparator
				.comparing(TestPojo::getValue1)
				// Field value2 and 3 can be null
				.thenComparing(TestPojo::getValue2, Comparator.nullsFirst(Comparator.naturalOrder()))
				.thenComparing(TestPojo::getValue3, Comparator.nullsFirst(Comparator.naturalOrder()));
		Collections.sort(testList, comparator);
		
		assertThat(testList).hasSize(7);
		assertThat(testList).containsExactly(
				new TestPojo("test1", null, 100L), new TestPojo("test1", 1, 100L), new TestPojo("test1", 2, 100L),
				new TestPojo("test2", 2, null), new TestPojo("test2", 2, 100L), new TestPojo("test2", 2, 200L),
				new TestPojo("test3", 3, 300L));
	}
	
	@RepeatedTest(5)
	public void reverseSortWithNullNumbersTest()
	{
		List<TestPojo> testList = createTestListWithNullNumbers();
		
		Comparator<TestPojo> comparator = Comparator
				.comparing(TestPojo::getValue1)
				// Field value2 and 3 can be null
				.thenComparing(TestPojo::getValue2, Comparator.nullsFirst(Comparator.naturalOrder()))
				.thenComparing(TestPojo::getValue3, Comparator.nullsFirst(Comparator.naturalOrder()))
				.reversed();
		Collections.sort(testList, comparator);
		
		assertThat(testList).hasSize(7);
		assertThat(testList).containsExactly(
				new TestPojo("test3", 3, 300L),
				new TestPojo("test2", 2, 200L), new TestPojo("test2", 2, 100L), new TestPojo("test2", 2, null),
				new TestPojo("test1", 2, 100L), new TestPojo("test1", 1, 100L), new TestPojo("test1", null, 100L));
	}
	
	/*------------------------------*
	 * SUPPORT METHODS AND CLASSES
	 *------------------------------*/
	public static List<TestPojo> createTestListWithNullStrings()
	{
		List<TestPojo> testList = new ArrayList<>();
		testList.add(new TestPojo("test1", 1, 100L));
		testList.add(new TestPojo("test1", 2, 100L));
		testList.add(new TestPojo("test1", 3, 100L));
		// --
		testList.add(new TestPojo("test2", 2, 100L));
		testList.add(new TestPojo("test2", 2, 200L));
		testList.add(new TestPojo("test2", 2, 300L));
		// --
		testList.add(new TestPojo("test3", 3, 300L));
		testList.add(new TestPojo(null, 4, 400L));
		
		Collections.shuffle(testList); 
		
		return testList;
	}
	
	public static List<TestPojo> createTestListWithNullNumbers()
	{
		List<TestPojo> testList = new ArrayList<>();
		testList.add(new TestPojo("test1", 1, 100L));
		testList.add(new TestPojo("test1", 2, 100L));
		testList.add(new TestPojo("test1", null, 100L));
		// --
		testList.add(new TestPojo("test2", 2, 100L));
		testList.add(new TestPojo("test2", 2, 200L));
		testList.add(new TestPojo("test2", 2, null));
		// --
		testList.add(new TestPojo("test3", 3, 300L));
		
		Collections.shuffle(testList); 
		
		return testList;
	}
	
	private static class TestPojo
	{
		private String value1;
		private Integer value2;
		private Long value3;
		
		public TestPojo(String value1, Integer value2, Long value3)
		{
			this.setValue1(value1);
			this.setValue2(value2);
			this.setValue3(value3);
		}
		
		public String getValue1() 
		{
			return value1;
		}
		
		public TestPojo setValue1(String value1) 
		{
			this.value1 = value1;
			return this;
		}
		
		public Integer getValue2() 
		{
			return value2;
		}
		
		public TestPojo setValue2(Integer value2) 
		{
			this.value2 = value2;
			return this;
		}
		
		public Long getValue3() 
		{
			return value3;
		}
		
		public TestPojo setValue3(Long value3) 
		{
			this.value3 = value3;
			return this;
		}

		@Override
		public String toString() 
		{
			StringBuilder builder = new StringBuilder();
			builder.append("TestPojo [value1=").append(value1)
					.append(", value2=").append(value2)
					.append(", value3=").append(value3).append("]");
			return builder.toString();
		}

		@Override
		public int hashCode() 
		{
			final int prime = 31;
			int result = 1;
			result = prime * result + ((value1 == null) ? 0 : value1.hashCode());
			result = prime * result + ((value2 == null) ? 0 : value2.hashCode());
			result = prime * result + ((value3 == null) ? 0 : value3.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) 
		{
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			
			TestPojo other = (TestPojo) obj;
			if (value1 == null) {
				if (other.value1 != null) {
					return false;
				}
			} 
			else if (!value1.equals(other.value1)) {
				return false;
			}
			if (value2 == null) {
				if (other.value2 != null) {
					return false;
				}
			} 
			else if (!value2.equals(other.value2)) {
				return false;
			}
			if (value3 == null) {
				if (other.value3 != null) {
					return false;
				}
			} 
			else if (!value3.equals(other.value3)) {
				return false;
			}
			
			return true;
		}
	}
}
