package com.bitsmi.ocp8.t7;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.Test;

public class SingleThreadExecutorTestCase 
{
	@Test
	public void executeTest() throws ExecutionException, InterruptedException
	{
		ExecutorService executor = Executors.newSingleThreadExecutor();
		
		try {
			/* Execute only works with Runnable interface. For lambda expressions, it doesn't allow return values, implicitly or explicitly
			 * This code will not compile because of lambda's implicit return 
			 */
//			executor.execute(() -> "TEST");
			// In this case, the lambda expression will not return any value
			executor.execute(() -> {String test = "TEST";});
			
			// Execute method doesn't return Future, so there is no way to check if submitted thread has finished   
		}
		finally {
			if(executor!=null) {
				executor.shutdown();
			}
		}
	}
	
	@Test
	public void submitTest() throws ExecutionException, InterruptedException
	{
		ExecutorService executor = Executors.newSingleThreadExecutor();
		try {
			// Callable<String>
			Future<String> callableFuture = executor.submit(() -> "done");
			// Runnable returns void, so lambda expressions musn't return ( or return;)
			AtomicInteger runnableResultCounter = new AtomicInteger(0);
			Future<?> runnableFuture = executor.submit(() -> runnableResultCounter.set(1));
			/* Unlike previous case, incrementAndGet returns a value, so even though there is no "return" in the lambda
			 * a value is returned and the submitted task is interpreted as Callable
			 */
			AtomicInteger falseRunnableResultCounter = new AtomicInteger(0);		
			Future<?> falseRunnableFuture = executor.submit(() -> falseRunnableResultCounter.incrementAndGet());
			// Throwing a checked exception also transforms the parameter into Callable
			Future<?> falseRunnableFutureWithException = executor.submit(() -> {throw new Exception();});
			
			String callableResult = callableFuture.get();
			// Runnable::run method returns void, so Future::get always returns null
			Object runnableResult = runnableFuture.get();
			// In that case the result will be different from null because the lambda had an implicit return that converts it in Callable
			Object falseRunnableResult = falseRunnableFuture.get();
			// If the computation throws an exception, it will be wrapped in a ExecutionException
			Throwable falseRunnableThrowable = catchThrowable(() -> {
				falseRunnableFutureWithException.get();
			});
			/* Callable */
			assertThat(callableFuture)
					.isDone()
					.isNotCancelled();
			assertThat(callableResult).isEqualTo("done");
			/* Runnable */
			assertThat(runnableFuture)
					.isDone()
					.isNotCancelled();
			assertThat(runnableResultCounter.get()).isEqualTo(1);
			assertThat(runnableResult).isNull();
			/* False Runnable */ 
			assertThat(falseRunnableFuture)
					.isDone()
					.isNotCancelled();
			assertThat(falseRunnableResultCounter.get()).isEqualTo(1);
			assertThat(falseRunnableResult).isEqualTo(1);
			
			assertThat(falseRunnableThrowable).isNotNull().isInstanceOf(ExecutionException.class);
		}
		finally {
			if(executor!=null) {
				executor.shutdown();
			}
		}
	}
}
