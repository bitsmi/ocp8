package com.bitsmi.ocp8.t4;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.DoubleSummaryStatistics;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

public class PrimitiveStreamTestCase 
{
	/**
	 * Applies to {@link IntStream}, {@link LongStream}, {@link DoubleStream}
	 */
	@Test
	public void createIntStreamTest()
	{
		
		IntStream is = IntStream.of(0, 1, 2, 3);
		IntStream iterateIs = IntStream.iterate(0, e -> e+1)
				// Limit to 4 first elements, so the infinite stream becomes finite
				.limit(4);
		AtomicInteger counter = new AtomicInteger(0);
		IntStream generateIs = IntStream.generate(counter::incrementAndGet)
				// Limit to 4 first elements, so the infinite stream becomes finite
				.limit(4);
		// Range and closed range streams doesn't apply to DoubleStream 
		IntStream rangeIs = IntStream.range(0, 4);
		IntStream closedRangeIs = IntStream.rangeClosed(0, 3);
		
		assertThat(is.count()).isEqualTo(4);
		assertThat(iterateIs.count()).isEqualTo(4);
		assertThat(generateIs.count()).isEqualTo(4);
		assertThat(rangeIs.count()).isEqualTo(4);
		assertThat(closedRangeIs.count()).isEqualTo(4);
	}
	
	@Test
	public void streamConversionTest()
	{
		String[] testValues = {"0", "1", "2", "3"};
		int[] testValuesInt = {0, 1, 2, 3};
		
		/* Stream -> IntStream, LongStream, DoubleStream */ 
		IntStream is = Stream.of(testValues).mapToInt(Integer::parseInt);
		LongStream ls = Stream.of(testValues).mapToLong(Long::parseLong);
		DoubleStream ds = Stream.of(testValues).mapToDouble(Double::parseDouble);

		/* IntStream -> LongStream, DoubleStream; LongStream -> IntStream, DoubleStream; DoubleStream -> IntStream, LongStream */
		LongStream i2ls = IntStream.of(testValuesInt).asLongStream();
		LongStream i2lsMapped = IntStream.of(testValuesInt).mapToLong(Integer::toUnsignedLong);
		/* In order to use Integer::longValue method as conversionFunction, the source primitive 
		 * stream must be transformed into a Stream with boxed primitives because it's not possible
		 * to call longValue method from a primitive.
		 */
		LongStream i2lsBoxed = IntStream.of(testValuesInt).boxed().mapToLong(Integer::longValue);
		
		/* IntStream, LongStream, DoubleSTream -> Stream */
		Stream<String> strs = IntStream.of(testValuesInt).mapToObj(Integer::toString);
		
		List<String> controlList = Arrays.asList(testValues);
		
		assertThat(is).hasSize(4).allMatch(e -> controlList.contains(e.toString()));
		assertThat(ls).hasSize(4).allMatch(e -> controlList.contains(e.toString()));
		assertThat(ds).hasSize(4).allMatch(e -> controlList.contains(Integer.toString(e.intValue())));
		assertThat(i2ls).hasSize(4).allMatch(e -> controlList.contains(e.toString()));
		assertThat(i2lsMapped).hasSize(4).allMatch(e -> controlList.contains(e.toString()));
		assertThat(i2lsBoxed).hasSize(4).allMatch(e -> controlList.contains(e.toString()));
		assertThat(strs).hasSize(4).allMatch(e -> controlList.contains(e));
	}
	
	@Test
	public void streamStatisticsTest()
	{
		int[] testValuesInt = {0, 1, 2, 3};
		long[] testValuesLong = {0, 1, 2, 3};
		double[] testValuesDouble = {0, 1, 2, 3};
		
		IntSummaryStatistics isStatistics = IntStream.of(testValuesInt).summaryStatistics();
		LongSummaryStatistics lsStatistics = LongStream.of(testValuesLong).summaryStatistics();
		DoubleSummaryStatistics dsStatistics = DoubleStream.of(testValuesDouble).summaryStatistics();
		
		/* Same for LongSummaryStatistics and DoubleSummaryStatistics */
		assertThat(isStatistics.getCount()).isEqualTo(4);
		assertThat(isStatistics.getMin()).isEqualTo(0);
		assertThat(isStatistics.getMax()).isEqualTo(3);
		assertThat(isStatistics.getSum()).isEqualTo(6);
		assertThat(isStatistics.getAverage()).isEqualTo(1.5);		
	}
	
	@Test
	public void finalOperationsTest()
	{
		int[] testValuesInt = {0, 1, 2, 3};
		
		boolean allMatch = IntStream.of(testValuesInt)
				.allMatch(e -> e<4);
		
		boolean noneMatchFinite = IntStream.of(testValuesInt)
				.noneMatch(e -> e>=4);
		// For infinite stream, stops on first match
		AtomicInteger noneMatchInfiniteCount = new AtomicInteger(0);
		boolean noneMatchInfinite = IntStream.iterate(0, e-> e+1)
				.peek(e -> noneMatchInfiniteCount.incrementAndGet())
				.noneMatch(e -> e>=4);
		
		AtomicInteger anyMatchFiniteCount = new AtomicInteger(0);
		boolean anyMatchFinite = IntStream.of(testValuesInt)
				.peek(e -> anyMatchFiniteCount.incrementAndGet())
				.anyMatch(e -> e==1);
		// For infinite streams, stops when anyMatch found a matching element
		AtomicInteger anyMatchInfiniteCount = new AtomicInteger(0);
		boolean anyMatchInfinite = IntStream.iterate(0, e-> e+1)
				.peek(e -> anyMatchInfiniteCount.incrementAndGet())
				.anyMatch(e -> e==1);

		OptionalInt firstFoundFinite = IntStream.of(testValuesInt)
				.findFirst();
		// For infinite streams, stops on first generated element (Same for findAny)
		AtomicInteger firstFoundInfiniteCount = new AtomicInteger(0);
		OptionalInt firstFoundInfinite = IntStream.iterate(0, e-> e+1)
				.peek(e -> firstFoundInfiniteCount.incrementAndGet())
				.findFirst();
		
		long finiteCount = IntStream.of(testValuesInt)
				.count();
		// For infinite streams, watch limit methods
		long infiniteCount = IntStream.of(testValuesInt)
				.limit(4)
				.count();
		
		int sum = IntStream.of(testValuesInt)
				.sum();
		OptionalDouble optAvg = IntStream.of(testValuesInt)
				.average();
		OptionalInt min = IntStream.of(testValuesInt)
				.min();
		OptionalInt max = IntStream.of(testValuesInt)
				.max();
		OptionalInt reduceSum = IntStream.of(testValuesInt)
				.reduce((e1, e2) -> e1+e2);
		
		assertThat(allMatch).isTrue();
		assertThat(noneMatchFinite).isTrue();
		assertThat(noneMatchInfinite).isFalse();
		assertThat(noneMatchInfiniteCount.get()).isEqualTo(5);
		assertThat(anyMatchFinite).isTrue();
		assertThat(anyMatchFiniteCount.get()).isEqualTo(2);
		assertThat(anyMatchInfinite).isTrue();
		assertThat(anyMatchInfiniteCount.get()).isEqualTo(2);
		assertThat(firstFoundFinite.getAsInt()).isEqualTo(0);
		assertThat(firstFoundInfinite.getAsInt()).isEqualTo(0);
		assertThat(firstFoundInfiniteCount.get()).isEqualTo(1);
		assertThat(finiteCount).isEqualTo(4);
		assertThat(infiniteCount).isEqualTo(4);
		assertThat(sum).isEqualTo(6);
		assertThat(optAvg.getAsDouble()).isEqualTo(1.5);
		assertThat(min.getAsInt()).isEqualTo(0);
		assertThat(max.getAsInt()).isEqualTo(3);
		assertThat(reduceSum.getAsInt()).isEqualTo(6);
	}
}
