package com.bitsmi.ocp8.t7;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

public class SingleScheduledExecutorTestCase 
{
	@Test
	public void scheduleTest() throws ExecutionException, InterruptedException
	{
		ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
		
		try {
			// One shoot tasks
			ScheduledFuture<String> scheduledCallableFuture = executor.schedule(() -> "done", 1, TimeUnit.SECONDS);
			ScheduledFuture<?> scheduledRunnableFuture = executor.schedule(() -> {}, 1, TimeUnit.SECONDS);
			
			String scheduledCallableResult = scheduledCallableFuture.get();
			Object scheduledRunnableResult = scheduledRunnableFuture.get();
			
			/* Callable */
			assertThat((Future<String>)scheduledCallableFuture)
					.isDone()
					.isNotCancelled();
			assertThat(scheduledCallableResult).isEqualTo("done");
			/* Runnable */
			assertThat((Future<?>)scheduledRunnableFuture)
					.isDone()
					.isNotCancelled();
			assertThat(scheduledRunnableResult).isNull();
		}
		finally {
			if(executor!=null) {
				executor.shutdown();
			}
		}
	}
	
	@Test
	public void scheduleAtFixedRateTest() throws ExecutionException, InterruptedException
	{
		final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
		
		try {
			CountDownLatch latch = new CountDownLatch(10);
			AtomicInteger counter = new AtomicInteger(0);
			Runnable runnable = () ->{		
				counter.incrementAndGet();
				latch.countDown();
			};
			
			executor.scheduleAtFixedRate(runnable, 1, 1, TimeUnit.SECONDS);

			/* Await until the specified number of tasks to be executed (10)
			 * For scheduled tasks, calling Future::get() on the object returned by schedule method will wait forever (if not cancelled)
			 */
			latch.await();
			
			assertThat(counter.get()).isEqualTo(10);
		}
		finally {
			// Shutdown method will stop executor from submit new scheduled tasks  
			if(executor!=null) {
				executor.shutdown();
			}
		}
	}
	
	@Test
	public void cancelScheduledTaskTest() throws ExecutionException, InterruptedException
	{
		final ScheduledExecutorService taksExecutor = Executors.newSingleThreadScheduledExecutor();
		final ScheduledExecutorService cancelExecutor = Executors.newSingleThreadScheduledExecutor();
		
		try {
			AtomicInteger counter = new AtomicInteger(0);
			Runnable runnable = () ->{		
				counter.incrementAndGet();
			};
			
			final ScheduledFuture<?> tasksFuture = taksExecutor.scheduleAtFixedRate(runnable, 1, 1, TimeUnit.SECONDS);
			cancelExecutor.schedule(() -> tasksFuture.cancel(true), 15, TimeUnit.SECONDS);

			Throwable throwable = catchThrowable(() -> {
				// Wait until task cancellation. A CancellationException will be thrown
				tasksFuture.get();
			});

			assertThat(counter.get()).isGreaterThan(10);
			assertThat(throwable).isNotNull().isInstanceOf(CancellationException.class);
		}
		finally {
			// Shutdown method will stop executor from submit new scheduled tasks  
			if(taksExecutor!=null) {
				taksExecutor.shutdown();
			}
			if(cancelExecutor!=null) {
				cancelExecutor.shutdown();
			}
		}
	}
	
	@Test
	public void invokeAllTest() throws InterruptedException
	{
		final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
		try {
			AtomicInteger counter = new AtomicInteger(0);
			Callable<Integer> task = () -> counter.incrementAndGet();
			
			Collection<Callable<Integer>> tasks = Arrays.asList(task, task, task);
			List<Future<Integer>> resultsFutures = executor.invokeAll(tasks);
			
			// Shutdown and wait for results
			executor.shutdown();
			executor.awaitTermination(10, TimeUnit.MICROSECONDS);
			
			List<Integer> results = resultsFutures.stream()
					.map(f -> {
						try {
							return f.get();
						}
						catch(ExecutionException | InterruptedException e) {
							throw new RuntimeException(e);
						}
					})
					.collect(Collectors.toList());
			
			assertThat(results).containsExactlyInAnyOrder(1, 2, 3);
			// All submitted tasks will be executed  
			System.out.println("[invokeAllTest] EXECUTED TASKS: " + counter.get());
			assertThat(counter.get()).isEqualTo(3);
		}
		finally {
			if(executor!=null) {
				executor.shutdown();
			}
		}
	}
	
	@Test
	public void invokeAnyTest() throws ExecutionException, InterruptedException
	{
		final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
		try {
			AtomicInteger counter = new AtomicInteger(0);
			Callable<Integer> task = () -> {
				Thread.sleep(1_000);
				return counter.incrementAndGet();
			};
			
			Collection<Callable<Integer>> tasks = Arrays.asList(task, task, task);
			Integer result = executor.invokeAny(tasks);
			
			// Shutdown and wait for results
			executor.shutdown();
			executor.awaitTermination(10, TimeUnit.MICROSECONDS);
			
			// For a single thread executor, the resulting value will always be the one returned by the first task
			assertThat(result).isEqualTo(1);
			// Submitted tasks may o may not be invoked for invokeAny  
			System.out.println("[invokeAnyTest] EXECUTED TASKS: " + counter.get());
			assertThat(counter.get()).isLessThanOrEqualTo(3);
		}
		finally {
			if(executor!=null) {
				executor.shutdown();
			}
		}
	}
}
