package com.bitsmi.ocp8.t3;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

public class MethodReferencesTestCase 
{
	/*------------------------------*
	 * STATIC METHOD REFERENCES
	 *------------------------------*/
	@Test
	public void staticPredicateTest()
	{
		List<String> testList = Arrays.asList("static_value1", "value2", "value3");

		List<String> referencesList = new ArrayList<>(testList);
		List<String> lambdaList = new ArrayList<>(testList);
		
		referencesList.removeIf(MethodReferencesTestCase::staticPredicate);
		lambdaList.removeIf(e -> staticPredicate(e));
		
		// To compare to Lists, their elements must be Comparable
		assertThat(referencesList).isEqualTo(lambdaList);
	}
	
	@Test
	public void staticUnaryOperatorTest()
	{
		List<String> testList = Arrays.asList("static_value1", "value2", "value3");

		List<String> referencesList = new ArrayList<>(testList);
		List<String> lambdaList = new ArrayList<>(testList);
		
		referencesList.replaceAll(MethodReferencesTestCase::staticOperator);
		lambdaList.replaceAll(e -> staticOperator(e));
		
		// To compare to Lists, their elements must be Comparable
		assertThat(referencesList).isEqualTo(lambdaList);
	}
	
	@Test
	public void staticFunctionOperatorTest()
	{
		List<String> testList = Arrays.asList("1", "2", "3");

		int referencesSum = testList.stream()			
				.collect(Collectors.summingInt(MethodReferencesTestCase::staticFunction));
		int lambdaSum = testList.stream()			
				.collect(Collectors.summingInt(e -> staticFunction(e)));
		
		// To compare to Lists, their elements must be Comparable
		assertThat(referencesSum).isEqualTo(6);
		assertThat(lambdaSum).isEqualTo(6);
	}
	
	@Test
	public void staticSupplierTest()
	{
		String value = null;
		
		String referencesValue = Optional.ofNullable(value).orElseGet(MethodReferencesTestCase::staticSupplier);
		String lambdaValue = Optional.ofNullable(value).orElseGet(() -> staticSupplier());
		
		assertThat(referencesValue).isEqualTo("suppliedStaticValue");
		assertThat(lambdaValue).isEqualTo("suppliedStaticValue");
	}
	
	/*------------------------------*
	 * INSTANCE METHOD REFERENCES
	 *------------------------------*/
	@Test
	public void instancePredicateTest()
	{
		List<String> testList = Arrays.asList("instance_value1", "value2", "value3");

		List<String> referencesList = new ArrayList<>(testList);
		List<String> lambdaList = new ArrayList<>(testList);
		
		InstanceFunctionals functionals = new InstanceFunctionals();
		
		referencesList.removeIf(functionals::instancePredicate);
		lambdaList.removeIf(e -> functionals.instancePredicate(e));
		
		// To compare to Lists, their elements must be Comparable
		assertThat(referencesList).isEqualTo(lambdaList);
	}
	
	@Test
	public void instanceOperatorOverloadedTest()
	{
		List<String> testStrList = Arrays.asList("value1", "value2", "value3");
		List<Integer> testIntList = Arrays.asList(1, 2, 3);
		
		InstanceFunctionals functionals = new InstanceFunctionals();
		// Calls String parameter variant
		testStrList.replaceAll(functionals::instanceOperator);
		// Calls Integer parameter variant
		testIntList.replaceAll(functionals::instanceOperator);
		
		assertThat(testStrList).containsExactly("value1_INSTANCE", "value2_INSTANCE", "value3_INSTANCE");
		assertThat(testIntList).containsExactly(101, 102, 103);
	}
	
	/*------------------------------*
	 * INSTANCE RUNTIME METHOD REFERENCES
	 *------------------------------*/
	@Test
	public void runtimeInstancePredicateTest()
	{
		List<FunctionalPojo> testList = Arrays.asList(new FunctionalPojo("value1"), new FunctionalPojo("invalid"), new FunctionalPojo("value3"));

		List<FunctionalPojo> referencesList = new ArrayList<>(testList);
		List<FunctionalPojo> lambdaList = new ArrayList<>(testList);
		
		referencesList.removeIf(FunctionalPojo::isInvalid);
		lambdaList.removeIf(e -> e.isInvalid());
		
		// To compare to Lists, their elements must be Comparable
		assertThat(referencesList).hasSize(2);
		assertThat(referencesList).extracting("value").contains("value1", "value3");
		assertThat(referencesList).isEqualTo(lambdaList);
	}
	
	@Test
	public void runtimeInstanceAndParameterTest()
	{
		List<String> testList = Arrays.asList("aaa", "bbb", "ccc", "ddd");
		
		BiPredicate<List<String>, String> pReference = List::contains;
		BiPredicate<List<String>, String> pLambda = (l, s) -> l.contains(s);
		
		boolean containsByReference = pReference.test(testList, "aaa");
		boolean containsByLambda = pLambda.test(testList, "aaa");
		boolean notContainsByReference = pReference.test(testList, "zzz");
		boolean notContainsByLambda = pReference.test(testList, "zzz");
		
		assertThat(containsByReference).isTrue();
		assertThat(containsByLambda).isTrue();
		assertThat(notContainsByReference).isFalse();
		assertThat(notContainsByLambda).isFalse();
	}
	
	/*------------------------------*
	 * CONSTRUCTOR REFERENCES
	 *------------------------------*/
	@Test
	public void constructorSupplierTest()
	{
		String value = null;
		
		String referencesValue = Optional.ofNullable(value).orElseGet(String::new);
		String lambdaValue = Optional.ofNullable(value).orElseGet(() -> new String());
		
		assertThat(referencesValue).isNotNull().isEmpty();
		assertThat(lambdaValue).isNotNull().isEmpty();
	}
	
	/*------------------------------*
	 * SUPPORT METHODS AND CLASSES
	 *------------------------------*/
	public static String staticOperator(String param)
	{
		return param + "_STATIC";
	}
	
	public static Integer staticFunction(String param)
	{
		return Integer.valueOf(param);
	}
	
	public static String staticSupplier()
	{
		return "suppliedStaticValue";
	}
	
	public static boolean staticPredicate(String value)
	{
		return value.startsWith("static");
	}
	
	public static class InstanceFunctionals
	{
		public String instanceOperator(String param)
		{
			return param + "_INSTANCE";
		}
		
		/* Overloaded method */
		public Integer instanceOperator(Integer param)
		{
			return param + 100;
		}
		
		public Integer instanceFunction(String param)
		{
			return param.length();
		}
		
		public String instanceSupplier()
		{
			return "suppliedInstanceValue";
		}
		
		public boolean instancePredicate(String value)
		{
			return value.startsWith("instance");
		}
	}
	
	public static class FunctionalPojo
	{
		private String value;
		
		public FunctionalPojo(String value)
		{
			this.value = value;
		}

		public String getValue() 
		{
			return value;
		}
		
		public boolean isInvalid()
		{
			return "invalid".equals(value);
		}

		public FunctionalPojo setValue(String value) 
		{
			this.value = value;
			return this;
		}
	}
}
