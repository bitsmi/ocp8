package com.bitsmi.ocp8.t3;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.Assertions.entry;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.junit.jupiter.api.Test;

public class NullableCollectionTestCase
{
    @Test
    public void linkedListNullTest()
    {
        List<String> list = new LinkedList<>();
        list.add(null);
        list.add("value1");
        list.add(null);
        
        assertThat(list).hasSize(3);
        assertThat(list).containsExactly(null, "value1", null);
    }
    
    @Test
    public void hashSetNullTest()
    {
        Set<String> set = new HashSet<>();
        set.add(null);
        set.add("value1");
        set.add(null);
        
        assertThat(set).hasSize(2);
        assertThat(set).containsExactly(null, "value1");
    }
    
    @Test
    public void treeSetNullTest()
    {
        Set<String> set = new TreeSet<>();
        Throwable thrown = catchThrowable(() -> {
            set.add(null);
            set.add("value1");
            set.add(null);
        });
        
        assertThat(thrown).isInstanceOf(NullPointerException.class);
    }
    
    @Test
    public void hashMapNullKeyTest()
    {
        Map<String, String> map = new HashMap<>();
        map.put(null, "value1");
        map.put("key2", "value2");
        map.put(null, "value3");
        
        assertThat(map).hasSize(2);
        assertThat(map).containsOnly(entry(null, "value3"), entry("key2", "value2"));
    }
    
    @Test
    public void treeMapNullKeyTest()
    {
        Map<String, String> map = new TreeMap<>();
        Throwable thrown = catchThrowable(() -> {
            map.put(null, "value1");
            map.put("key2", "value2");
            map.put(null, "value3");
        });
        
        assertThat(thrown).isInstanceOf(NullPointerException.class);
    }
}
