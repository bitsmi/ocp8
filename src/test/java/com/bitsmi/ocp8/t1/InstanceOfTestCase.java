package com.bitsmi.ocp8.t1;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class InstanceOfTestCase 
{
	@Test
	public void instanceOfTest()
	{
		TestClass testClassObj = new TestClass();
		TestInterface1 testI1Obj = new TestClass();
		TestClass nullClassObj = null;
		TestInterface1 nullI1Obj = null;
		
		assertThat(testClassObj instanceof TestInterface1).isTrue();
		assertThat(testClassObj instanceof TestClass).isTrue();
		assertThat(testClassObj instanceof TestExtendedClass).isFalse();
		// Unrelated hierarchy but second operand is an interface -> Compilation OK
		assertThat(testClassObj instanceof TestInterface2).isFalse();
		// Null references always return false
		assertThat(nullClassObj instanceof TestInterface1).isFalse();
		// When object  nullI1Obj null  
		// vnullI1Obj instanceof TestInterface1 ----> ALWAYS False not null.
		assertThat(nullI1Obj instanceof TestInterface1).isFalse();
		
		
		assertThat(testI1Obj instanceof TestInterface1).isTrue();
		assertThat(testI1Obj instanceof TestClass).isTrue();
		assertThat(testI1Obj instanceof TestExtendedClass).isFalse();
		// Unrelated hierarchy but second operand is an interface -> Compilation OK
		assertThat(testClassObj instanceof TestInterface2).isFalse();
		
		// Unrelated class hierarchy -> Compilation Error 
//		assertThat(testClassObj instanceof String).isFalse();
		// String doesn't implements TestInterface1 -> Compilation Error
//		assertThat(testI1Obj instanceof String).isFalse();
	}
	
	/*------------------------------*
	 * SUPPORT METHODS AND CLASSES
	 *------------------------------*/
	private static interface TestInterface1 { } 
	private static class TestClass implements TestInterface1 { }
	private static class TestExtendedClass extends TestClass { } 
	
	private static interface TestInterface2 { }
}
