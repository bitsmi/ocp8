package com.bitsmi.ocp8.t5;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import org.junit.jupiter.api.Test;

public class ZonedDateTimeTestCase 
{
	@Test
	public void timezoneDiffTest()
	{
		LocalDate referenceDate = LocalDate.of(2019, 2, 1);
		LocalTime referenceTime = LocalTime.of(12, 0);
		
		ZonedDateTime zonedDTGMTp1 = ZonedDateTime.of(referenceDate, referenceTime, ZoneId.of("GMT+1")); 
		ZonedDateTime zonedDTGMTm1 = ZonedDateTime.of(referenceDate, referenceTime, ZoneId.of("GMT-1"));
		
		long diffHoursByDuration = Duration.between(zonedDTGMTm1, zonedDTGMTp1).toHours();
		
		Instant im1 = zonedDTGMTm1.toInstant();
		Instant ip1 = zonedDTGMTp1.toInstant();
		long diffHoursByInstants = ChronoUnit.HOURS.between(im1, ip1);
		
		assertThat(diffHoursByDuration).isEqualTo(-2);
		assertThat(diffHoursByInstants).isEqualTo(-2);
	}
	
	@Test
	public void zoneShiftTest()
	{
		LocalDate referenceDate = LocalDate.of(2019, 2, 1);
		LocalTime referenceTime = LocalTime.of(12, 0);
		
		ZonedDateTime referenceZDT = ZonedDateTime.of(referenceDate, referenceTime, ZoneId.of("GMT+1"));
		ZonedDateTime shiftedZDT = referenceZDT.withZoneSameInstant(ZoneId.of("GMT-1"));
		
		assertThat(shiftedZDT.getHour()).isEqualTo(10);
	}
	
	@Test
	public void dstTest()
	{
		LocalDate date = LocalDate.of(2019, 2, 1);
		LocalTime time = LocalTime.of(12, 0);
		
		ZonedDateTime localWinterZDT = ZonedDateTime.of(date, time, ZoneId.of("Europe/Madrid"));
		ZonedDateTime referenceWinterZDT = ZonedDateTime.of(date, time, ZoneId.of("GMT+1"));
		
		ZonedDateTime localSummerZDT = localWinterZDT.plus(8, ChronoUnit.MONTHS);
		ZonedDateTime referenceSummerZDT = referenceWinterZDT.plus(8, ChronoUnit.MONTHS);
		
		long diffWinter = ChronoUnit.HOURS.between(localWinterZDT.toInstant(), referenceWinterZDT.toInstant());
		long diffSummer = ChronoUnit.HOURS.between(localSummerZDT.toInstant(), referenceSummerZDT.toInstant());
		
		assertThat(diffWinter).isEqualTo(0);
		assertThat(diffSummer).isEqualTo(1);
	}
}
