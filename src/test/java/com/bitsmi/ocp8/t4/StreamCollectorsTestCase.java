package com.bitsmi.ocp8.t4;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

public class StreamCollectorsTestCase 
{
	@Test
	public void listCollectionTest()
	{
		String[] testValues = {"aaa", "aab", "bbb", "ccc"};
		
		/* Supplier<Container>, BiConsumer<Container, Elem>, BiConsumer<Container, Container>
		 * Container -> List<Testpojo>
		 * Elem -> TestPojo
		 */
		List<String> genericCollectResult = Stream.of(testValues)
				.collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
		
		List<String> collectorResult = Stream.of(testValues)
				.collect(Collectors.toList());
		
		assertThat(genericCollectResult).hasSize(4);
		assertThat(genericCollectResult).containsExactly("aaa", "aab", "bbb", "ccc");
		
		assertThat(collectorResult).hasSize(4);
		assertThat(collectorResult).containsExactly("aaa", "aab", "bbb", "ccc");
	}
	
	@Test
	public void mapCollectionTest()
	{
		TestPojo[] testValues = { 
				new TestPojo("test1", 1, 100L),
				new TestPojo("test2", 2, 200L),
				new TestPojo("test3", 3, 300L),
				new TestPojo("test4", 4, 400L)
		};
		
		/* Supplier<Container>, BiConsumer<Container, Elem>, BiConsumer<Container, Container>
		 * Container -> Map<String, Testpojo>
		 * Elem -> TestPojo
		 */
		Map<String, TestPojo> genericCollectResult = Stream.of(testValues)
				.collect(HashMap::new, (m, e) -> m.put(e.getValue1(), e), HashMap::putAll);
		
		Map<String, TestPojo> collectorResult = Stream.of(testValues)
				.collect(Collectors.toMap(TestPojo::getValue1, Function.identity()));
		
		assertThat(genericCollectResult).hasSize(4);
		assertThat(genericCollectResult).containsOnly(
				entry("test1", testValues[0]), 
				entry("test2", testValues[1]), 
				entry("test3", testValues[2]), 
				entry("test4", testValues[3]));
		assertThat(collectorResult).hasSize(4);
		assertThat(collectorResult).containsOnly(
				entry("test1", testValues[0]), 
				entry("test2", testValues[1]), 
				entry("test3", testValues[2]), 
				entry("test4", testValues[3]));
	}
	
	@Test
	public void mergeMapCollectorTest()
	{
		TestPojo[] testValues = { 
				new TestPojo("test1", 1, 100L),
				new TestPojo("test2", 2, 200L),
				new TestPojo("test2", 22, 2200L),
				new TestPojo("test3", 3, 300L)
		};
		
		/* Collectors.toMap requires unique keys. If there are repeated keys, an exception will be thrown. 
		 * In that case, a merge BinaryOperator is required to resolve the conflict.
		 * In the example, the second element is discarded 
		 */
		Map<String, TestPojo> collectorResult = Stream.of(testValues)
				.collect(Collectors.toMap(TestPojo::getValue1, Function.identity(), (e1, e2) -> e1));
		
		assertThat(collectorResult).hasSize(3);
		assertThat(collectorResult).containsOnly(
				entry("test1", testValues[0]), 
				entry("test2", testValues[1]), 
				entry("test3", testValues[3]));
	}
	
	@Test
	public void groupByTest()
	{
		TestPojo[] testValues = { 
				new TestPojo("test1", 1, 100L),
				new TestPojo("test1", 11, 1100L),
				new TestPojo("test2", 2, 200L),
				new TestPojo("test2", 22, 2200L),
				new TestPojo("test3", 3, 300L)
		};
		
		/* Supplier<Container>, BiConsumer<Container, Elem>, BiConsumer<Container, Container>
		 * Container -> Map<String, List<Testpojo>
		 * Elem -> TestPojo
		 */
		Map<String, List<TestPojo>> genericCollectResult = Stream.of(testValues)
				.collect(HashMap::new, 
						(m, e) ->{
							if(!m.containsKey(e.getValue1())){
								m.put(e.getValue1(), new ArrayList<>());
							}
							m.get(e.getValue1()).add(e);
						},
						HashMap::putAll
				);
		
		Map<String, List<TestPojo>> collectorResult = Stream.of(testValues)
				.collect(Collectors.groupingBy(pojo -> pojo.getValue1()));
		
		assertThat(genericCollectResult).hasSize(3);
		assertThat(genericCollectResult.get("test1")).containsOnly(testValues[0], testValues[1]);
		assertThat(genericCollectResult.get("test2")).containsOnly(testValues[2], testValues[3]);
		assertThat(genericCollectResult.get("test3")).containsOnly(testValues[4]);
		assertThat(collectorResult).hasSize(3);
		assertThat(collectorResult.get("test1")).containsOnly(testValues[0], testValues[1]);
		assertThat(collectorResult.get("test2")).containsOnly(testValues[2], testValues[3]);
		assertThat(collectorResult.get("test3")).containsOnly(testValues[4]);
	}
	
	@Test
	public void mappingCollectorTest()
	{
		TestPojo[] testValues = { 
				new TestPojo("test1", 1, 100L),
				new TestPojo("test2", 2, 200L),
				new TestPojo("test3", 3, 300L),
				new TestPojo("test4", 4, 400L)
		};
		
		// Performs map + collect operation
		List<String> collectorResult = Stream.of(testValues)
				.collect(Collectors.mapping(e -> e.getValue1(), Collectors.toList()));
		
		assertThat(collectorResult).hasSize(4);
		assertThat(collectorResult).containsOnly("test1", "test2", "test3", "test4");
	}
	
	@Test
	public void collectorReduceTest()
	{
		TestPojo[] testValues = { 
				new TestPojo("test1", 1, 100L),
				new TestPojo("test2", 2, 200L),
				new TestPojo("test3", 3, 300L)
		};
		
		/* Generic collect example
		 * Supplier<Container>, BiConsumer<Container, Elem>, BiConsumer<Container, Container>
		 * Container -> Integer[]
		 * Elem -> Integer
		 */
		Integer[] auxArray = Stream.of(testValues)
				.map(TestPojo::getValue2)
				.collect(() -> new Integer[] {0},			// 0 identity value
						(c, e) -> c[0] = c[0]+e,			// Accumulate
						(c1, c2) -> c1[0] = c1[0]+c2[0]);	// Merge
		Integer genericResult = auxArray[0];
		
		/* Performs a sum operation using a reduction using a identity value. 
		 * In that case the returned value is always of type <T>,  even though the stream is empty
		 * A intermediate operation is needed to extract value2 from pojo
		 */
		Integer collectorResultWithIdentity = Stream.of(testValues)
				.map(TestPojo::getValue2)
				.collect(Collectors.reducing(0, (e1, e2) -> e1+e2));
		/* Performs a sum operation using a reduction WITHOUT an identity value. 
		 * In that case the returned value is an Optional<T> because the Stream may be empty
		 * A intermediate operation is needed to extract value2 from pojo
		 */
		Optional<Integer> collectorResultWithoutIdentity = Stream.of(testValues)
				.map(TestPojo::getValue2)
				.collect(Collectors.reducing((e1, e2) -> e1+e2));
		/* Performs a sum operation using a reduction using an identity value. 
		 * In that case the returned value is always of type <T>,  even though the stream is empty
		 * The map operation is integrated into Collector's factory method call, so the intermediate operation is no needed
		 */
		Integer collectorResultWithMap = Stream.of(testValues)
				.collect(Collectors.reducing(0, TestPojo::getValue2, (e1, e2) -> e1+e2));
		
		/* Test reducing operations in empty streams */
		Integer collectorResultEmptyWithIdentity = Stream.<Integer>empty()
				.collect(Collectors.reducing(1, (e1, e2) -> e1*e2));
		Optional<Integer> collectorResultEmptyWithoutIdentity = Stream.<Integer>empty()
				.collect(Collectors.reducing((e1, e2) -> e1*e2));
		Integer collectorResultEmptyWithMap = Stream.<String>empty()
				.collect(Collectors.reducing(1, Integer::valueOf, (e1, e2) -> e1*e2));
		
		// Assert generic collect operation results
		assertThat(genericResult).isEqualTo(6);
		// Assert Collectors methods results
		assertThat(collectorResultWithIdentity).isEqualTo(6);
		assertThat(collectorResultWithoutIdentity.isPresent()).isTrue();
		assertThat(collectorResultWithoutIdentity.get()).isEqualTo(6);
		assertThat(collectorResultWithMap).isEqualTo(6);
		// Assert empty stream results
		assertThat(collectorResultEmptyWithIdentity).isEqualTo(1);
		assertThat(collectorResultEmptyWithoutIdentity.isPresent()).isFalse();
		assertThat(collectorResultEmptyWithMap).isEqualTo(1);
	}
	
	@Test
	public void partitioningTest()
	{
		TestPojo[] testValues = { 
				new TestPojo("false", 1, 100L),
				new TestPojo("true", 2, 200L),
				new TestPojo("true", 3, 300L)
		};
		
		/* Make partitions by value1. The result is a list of related values divided into 2 groups (true, false)
		 * according to the predicate evaluation for each element of the stream 
		 */
		Map<Boolean, List<TestPojo>> collectorResult = Stream.of(testValues)
				.collect(Collectors.partitioningBy(e -> Boolean.valueOf(e.getValue1())));
		
		// Make partitions by value1 and reduce each group by value2
		Map<Boolean, Integer> reducingCollectorResult = Stream.of(testValues)
				.collect(Collectors.partitioningBy(e -> Boolean.valueOf(e.getValue1()), 
						Collectors.reducing(0, TestPojo::getValue2, (e1, e2) -> e1+e2)));
		
		assertThat(collectorResult.get(Boolean.FALSE)).hasSize(1);
		assertThat(collectorResult.get(Boolean.FALSE)).containsOnly(testValues[0]);
		assertThat(collectorResult.get(Boolean.TRUE)).hasSize(2);
		assertThat(collectorResult.get(Boolean.TRUE)).containsOnly(testValues[1], testValues[2]);
		
		assertThat(reducingCollectorResult.get(Boolean.FALSE)).isEqualTo(1);
		assertThat(reducingCollectorResult.get(Boolean.TRUE)).isEqualTo(5);
	}
	
	@Test
	public void sumAndStatisticsTest()
	{
		TestPojo[] testValues = { 
				new TestPojo("false", 1, 100L),
				new TestPojo("true", 2, 200L),
				new TestPojo("true", 3, 300L)
		};
		
		Long longSumResult = Stream.of(testValues)
				.collect(Collectors.summingLong(TestPojo::getValue3));
		// Empty stream -> 0
		Long longSumEmptyResult = Stream.<TestPojo>empty()
				.collect(Collectors.summingLong(TestPojo::getValue3));
		
		LongSummaryStatistics longStatisticsResult = Stream.of(testValues)
				.collect(Collectors.summarizingLong(TestPojo::getValue3));
		
		// Assert sum results
		assertThat(longSumResult).isEqualTo(600);
		// Assert empty stream results
		assertThat(longSumEmptyResult).isEqualTo(0);
		// Assert summary results
		assertThat(longStatisticsResult.getCount()).isEqualTo(3);
		assertThat(longStatisticsResult.getSum()).isEqualTo(600);
		assertThat(longStatisticsResult.getMax()).isEqualTo(300);
		assertThat(longStatisticsResult.getMin()).isEqualTo(100);
		assertThat(longStatisticsResult.getAverage()).isEqualTo(200);
	}
	
	/*------------------------------*
	 * SUPPORT METHODS AND CLASSES
	 *------------------------------*/
	private static class TestPojo
	{
		private String value1;
		private Integer value2;
		private Long value3;
		
		public TestPojo(String value1, Integer value2, Long value3)
		{
			this.setValue1(value1);
			this.setValue2(value2);
			this.setValue3(value3);
		}
		
		public String getValue1() 
		{
			return value1;
		}
		
		public TestPojo setValue1(String value1) 
		{
			this.value1 = value1;
			return this;
		}
		
		public Integer getValue2() 
		{
			return value2;
		}
		
		public TestPojo setValue2(Integer value2) 
		{
			this.value2 = value2;
			return this;
		}
		
		public Long getValue3() 
		{
			return value3;
		}
		
		public TestPojo setValue3(Long value3) 
		{
			this.value3 = value3;
			return this;
		}

		@Override
		public String toString() 
		{
			StringBuilder builder = new StringBuilder();
			builder.append("TestPojo [value1=").append(value1)
					.append(", value2=").append(value2)
					.append(", value3=").append(value3).append("]");
			return builder.toString();
		}

		@Override
		public int hashCode() 
		{
			final int prime = 31;
			int result = 1;
			result = prime * result + ((value1 == null) ? 0 : value1.hashCode());
			result = prime * result + ((value2 == null) ? 0 : value2.hashCode());
			result = prime * result + ((value3 == null) ? 0 : value3.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) 
		{
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			
			TestPojo other = (TestPojo) obj;
			if (value1 == null) {
				if (other.value1 != null) {
					return false;
				}
			} 
			else if (!value1.equals(other.value1)) {
				return false;
			}
			if (value2 == null) {
				if (other.value2 != null) {
					return false;
				}
			} 
			else if (!value2.equals(other.value2)) {
				return false;
			}
			if (value3 == null) {
				if (other.value3 != null) {
					return false;
				}
			} 
			else if (!value3.equals(other.value3)) {
				return false;
			}
			
			return true;
		}
	}
}
