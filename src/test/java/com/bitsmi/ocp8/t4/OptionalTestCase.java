package com.bitsmi.ocp8.t4;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;
import java.util.function.Supplier;

import org.junit.jupiter.api.Test;

public class OptionalTestCase 
{
	@Test
	public void nullValueOptionalTest()
	{
		String value = null;
		Throwable nullThrown = catchThrowable(() -> {
			Optional.of(value);			
		});
		
		assertThat(nullThrown).isInstanceOf(NullPointerException.class);
	}
	
	@Test
	public void nullableOptionalTest()
	{
		String value = null;
		
		Optional<String> optNullableValue = Optional.ofNullable(value);
		boolean valuePresent = optNullableValue.isPresent();
		Throwable getThrown = catchThrowable(() -> {
			optNullableValue.get();
		});
		
		assertThat(valuePresent).isFalse();
		assertThat(getThrown).isInstanceOf(NoSuchElementException.class);
	}
	
	@Test
	public void emptyOptionalTest()
	{
		Optional<String> optEmpty = Optional.empty();
		boolean valuePresent = optEmpty.isPresent();
		Throwable getThrown = catchThrowable(() -> {
			optEmpty.get();
		});
		
		assertThat(valuePresent).isFalse();
		assertThat(getThrown).isInstanceOf(NoSuchElementException.class);
	}
	
	@Test
	public void orElseMethodsTest() throws Exception
	{
		String testValue = "value";
		String defaultValue = "default_value";
		String suppliedValue = "supplied_value";
		
		Optional<String> optNonEmpty = Optional.of(testValue);
		Optional<String> optEmpty = Optional.empty();
		
		Supplier<String> valueSupplier = () -> suppliedValue;
		String suppliedNonEmpty = optNonEmpty.orElseGet(valueSupplier);
		String suppliedEmpty = optEmpty.orElseGet(valueSupplier);
		
		String elseValueNonEmpty = optNonEmpty.orElse(defaultValue);
		String elseValueEmpty = optEmpty.orElse(defaultValue);
		
		Supplier<Exception> exceptionSupplier = () -> new RuntimeException();
		String throwValueNonEmpty = optNonEmpty.orElseThrow(exceptionSupplier);
		Throwable thrownEmpty = catchThrowable(() -> {
			optEmpty.orElseThrow(exceptionSupplier);
		});
		
		assertThat(suppliedNonEmpty).isEqualTo(testValue);
		assertThat(suppliedEmpty).isEqualTo(suppliedValue);
		assertThat(elseValueNonEmpty).isEqualTo(testValue);
		assertThat(elseValueEmpty).isEqualTo(defaultValue);
		assertThat(throwValueNonEmpty).isEqualTo(testValue);
		assertThat(thrownEmpty).isInstanceOf(RuntimeException.class);
	}
	
	@Test 
	public void filterTest()
	{
		Optional<String> optNonEmpty = Optional.of("test_value");
		Optional<String> optNonEmptyAlt = Optional.of("test_value_alt");
		Optional<String> optEmpty = Optional.empty();
		
		Predicate<String> predicate = e -> e.endsWith("_alt");
		Optional<String> optNonEmptyFiltered = optNonEmpty.filter(predicate);
		Optional<String> optNonEmptyAltFiltered = optNonEmptyAlt.filter(predicate);
		Optional<String> optEmptyFiltered = optEmpty.filter(predicate);
		
		assertThat(optNonEmptyFiltered.isPresent()).isFalse();
		assertThat(optNonEmptyFiltered.orElseGet(() -> null)).isNull();
		assertThat(optNonEmptyAltFiltered.isPresent()).isTrue();
		assertThat(optNonEmptyAltFiltered.orElseGet(() -> null)).isEqualTo("test_value_alt");
		assertThat(optEmptyFiltered.isPresent()).isFalse();		
		assertThat(optEmptyFiltered.orElseGet(() -> null)).isNull();
	}
	
	@Test
	public void presentConsumerTest()
	{
		Optional<String> optNonEmpty = Optional.of("test_value");
		Optional<String> optEmpty = Optional.empty();
		
		AtomicBoolean nonEmptyPresent = new AtomicBoolean(false); 
		optNonEmpty.ifPresent(e -> nonEmptyPresent.set(true));
		AtomicBoolean emptyPresent = new AtomicBoolean(false);
		optEmpty.ifPresent(e -> emptyPresent.set(true));
		
		assertThat(nonEmptyPresent.get()).isTrue();
		assertThat(emptyPresent.get()).isFalse();
	}
	
	@Test
	public void mapTest()
	{
		Optional<TestPojo> optNonEmpty = Optional.of(new TestPojo("value_1"));
		Optional<TestPojo> optEmpty = Optional.empty();
		
		// Value returned from Function is wrapped automatically into a Optional
		Optional<String> nonEmptyValue = optNonEmpty.map(e -> e.getValue1());
		Optional<String> emptyValue = optEmpty.map(e -> e.getValue1());
		
		// Value returned from Function is NOT wrapped into a Optional
		Optional<String> nonEmptyValueFlat = optNonEmpty.flatMap(e -> Optional.of(e.getValue1()));
		Optional<String> emptyValueFlat = optEmpty.flatMap(e -> Optional.of(e.getValue1()));
		
		// Map
		assertThat(nonEmptyValue.isPresent()).isTrue();
		assertThat(nonEmptyValue.get()).isEqualTo("value_1");
		assertThat(emptyValue.isPresent()).isFalse();
		// Flat map
		assertThat(nonEmptyValueFlat.isPresent()).isTrue();
		assertThat(nonEmptyValueFlat.get()).isEqualTo("value_1");
		assertThat(emptyValueFlat.isPresent()).isFalse();
	}
	
	private static class TestPojo
	{
		private String value1;
		
		public TestPojo(String value1) 
		{
			this.value1 = value1;
		}
		
		public String getValue1() 
		{
			return value1;
		}
	}
}
