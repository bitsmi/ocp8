package com.bitsmi.ocp8.t7;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.junit.jupiter.api.Test;

public class LocksAndBarriersTestCase 
{
	@Test
	public void latchTest() throws InterruptedException
	{
		ScheduledExecutorService executorService = Executors.newScheduledThreadPool(3);
		
		CountDownLatch latch = new CountDownLatch(3);
		AtomicInteger counter = new AtomicInteger(0);
		
		Runnable task = () -> {
			counter.getAndIncrement();
			latch.countDown();
		};
		
		executorService.scheduleAtFixedRate(task, 100, 100, TimeUnit.MILLISECONDS);
		
		// Throws InterruptedException
		latch.await();
		
		assertThat(counter).hasValue(3);
	}
	
	@Test
	public void cyclicBarrierTest() throws InterruptedException
	{
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		
		CyclicBarrier barrier1 = new CyclicBarrier(2);
		CyclicBarrier barrier2 = new CyclicBarrier(2);
		List<String> sequence = Collections.synchronizedList(new ArrayList<String>());
		
		Runnable task = () -> {
			try {
				sequence.add("B1");
				barrier1.await();
				sequence.add("B2");
				barrier2.await();
				sequence.add("B3");
			}
			catch(BrokenBarrierException | InterruptedException e) {
				throw new RuntimeException("ERROR", e);
			}
			finally {
				executorService.shutdown();
			}
		};
		
		executorService.submit(task);
		executorService.submit(task);
		
		// Wait for all task termination. May throw InterruptedException
		executorService.awaitTermination(1000, TimeUnit.MILLISECONDS);
		
		assertThat(sequence).containsExactly("B1", "B1", "B2", "B2", "B3", "B3");
	}
	
	@Test
	public void cyclicBarrierCallbackTest() throws InterruptedException
	{
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		
		AtomicInteger flag = new AtomicInteger(0);
		AtomicInteger counter = new AtomicInteger(0);
		
		// The Runnable callback will be executed when barrier's await counter reach 0
		CyclicBarrier barrier = new CyclicBarrier(2, () -> flag.getAndIncrement());
		
		Runnable task = () -> {
			try {
				barrier.await();
				counter.getAndIncrement();
			}
			catch(BrokenBarrierException | InterruptedException e) {
				throw new RuntimeException("ERROR", e);
			}
			finally {
				executorService.shutdown();
			}
		};
		
		executorService.submit(task);
		executorService.submit(task);
		
		// Wait for all task termination. May throw InterruptedException
		executorService.awaitTermination(1000, TimeUnit.MILLISECONDS);
		
		assertThat(counter).hasValue(2);
		assertThat(flag).hasValue(1);
	}
	
	@Test
	public void reentrantLockTest() throws InterruptedException
	{
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		
		ReentrantLock lock = new ReentrantLock();
		List<String> sequence = Collections.synchronizedList(new ArrayList<String>());
		
		Runnable task = () -> {
			try {
				// Wait until lock is available. tryLock() method will always return immediately
				lock.lock();
				sequence.add("L1");
				sequence.add("L2");
				sequence.add("L3");
				lock.unlock();
			}
			finally {
				executorService.shutdown();
			}
		};
		
		executorService.submit(task);
		executorService.submit(task);
		
		// Wait for all task termination. May throw InterruptedException
		executorService.awaitTermination(1000, TimeUnit.MILLISECONDS);
		
		assertThat(sequence).containsExactly("L1", "L2", "L3", "L1", "L2", "L3");
	}
	
	@Test
	public void reentrantRwLockTest() throws InterruptedException
	{
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		
		ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
		List<String> sequence = Collections.synchronizedList(new ArrayList<String>());
		
		Runnable task = () -> {
			try {
				while(!lock.writeLock().tryLock());
				sequence.add("W1");
				Thread.sleep(10);
				sequence.add("W2");
				Thread.sleep(10);
				sequence.add("W3");
				Thread.sleep(10);
				lock.writeLock().unlock();
				
				while(!lock.readLock().tryLock());
				sequence.add("R1");
				Thread.sleep(10);
				sequence.add("R2");
				Thread.sleep(10);
				sequence.add("R3");
				Thread.sleep(10);
				lock.readLock().unlock();
			}
			catch(InterruptedException e) {
				throw new RuntimeException("ERROR", e);
			}
			finally {
				executorService.shutdown();
			}
		};
		
		executorService.submit(task);
		executorService.submit(task);
		
		// Wait for all task termination. May throw InterruptedException
		executorService.awaitTermination(1000, TimeUnit.MILLISECONDS);
		
		// E.G. [W1, W2, W3, W1, W2, W3, R1, R1, R2, R2, R3, R3]
		System.out.println(sequence);
	}
}
