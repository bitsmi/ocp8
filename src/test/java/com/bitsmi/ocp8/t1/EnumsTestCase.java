package com.bitsmi.ocp8.t1;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import org.junit.jupiter.api.Test;

public class EnumsTestCase 
{
	@Test
	public void equalityTest()
	{
		SimpleTestEnum testValue1 = SimpleTestEnum.VALUE1;
		SimpleTestEnum testValue2 = SimpleTestEnum.VALUE2;
		
		assertThat(SimpleTestEnum.VALUE1.equals(testValue1)).isTrue();
		assertThat(SimpleTestEnum.VALUE1.equals(testValue2)).isFalse();
		assertThat(SimpleTestEnum.VALUE1==testValue1).isTrue();
		// Compilation error
//		assertThat(SimpleTestEnum.VALUE1==0).isTrue();
	}
	
	@Test
	public void switchStatementTest()
	{
		int result = 0;
		SimpleTestEnum value = SimpleTestEnum.VALUE1;
		
		/* If switch operand is an enum, case expressions test values 
		 * doesn't indicate the actual enum name, only the value.
		 * Enum name is inferred from the test value
		 */
		switch(value) {
			case VALUE1: 
				result = 1;
				break;
			case VALUE2: 
				result = 2;
				break;
			default: 
				result = 99;
		}
		
		assertThat(result).isEqualTo(1);
	}
	
	@Test
	public void enumInstanceTest()
	{
		assertThat(SimpleTestEnum.VALUE1 instanceof Object).isTrue();
		assertThat(SimpleTestEnum.VALUE1 instanceof Enum).isTrue();
		assertThat(SimpleTestEnum.VALUE1 instanceof SimpleTestEnum).isTrue();
		// Unrelated class hierarchy -> Compilation Error
//		assertThat(SimpleTestEnum.VALUE1 instanceof String).isTrue();
	}
	
	@Test
	public void valueOfTest()
	{
		SimpleTestEnum byNameEnum = SimpleTestEnum.valueOf("VALUE1");
		Throwable wrongByNameEnumThrown = catchThrowable(() -> {
			// Case sensitive
			SimpleTestEnum.valueOf("value1");
		});
		Throwable nullByNameEnumThrown = catchThrowable(() -> {
			SimpleTestEnum.valueOf(null);
		});
		
		assertThat(SimpleTestEnum.VALUE1.toString()).isEqualTo("VALUE1");
		assertThat(byNameEnum).isEqualTo(SimpleTestEnum.VALUE1);
		assertThat(byNameEnum).isNotEqualTo(SimpleTestEnum.VALUE2);
		
		assertThat(wrongByNameEnumThrown).isInstanceOf(IllegalArgumentException.class);
		assertThat(nullByNameEnumThrown).isInstanceOf(NullPointerException.class);
	}
	
	@Test
	public void valuesTest()
	{
		SimpleTestEnum[] values = SimpleTestEnum.values();
		
		assertThat(values).hasSize(2);
		assertThat(SimpleTestEnum.VALUE1.ordinal()).isEqualTo(0);
		assertThat(SimpleTestEnum.VALUE2.ordinal()).isEqualTo(1);
	}
	
	@Test
	public void constructorAndMethodsTest()
	{
		ComplexTestEnum testValue1 = ComplexTestEnum.VALUE1;
		ComplexTestEnum testValue2 = ComplexTestEnum.VALUE2;
		
		assertThat(testValue1.getTestValueName()).isEqualTo(ComplexTestEnum.CONSTANT_VALUE1);
		assertThat(testValue2.getTestValueName()).isEqualTo(ComplexTestEnum.CONSTANT_VALUE2 + " OVERRIDE");
		assertThat(ComplexTestEnum.getStaticText()).isEqualTo(ComplexTestEnum.CONSTANT_STATIC_TEXT);
	}
	
	/*------------------------------*
	 * SUPPORT METHODS AND CLASSES
	 *------------------------------*/
	private static enum SimpleTestEnum 
	{
		VALUE1,
		VALUE2
	}
	
	private static enum ComplexTestEnum
	{
		// Compilation error. Enum values must be the first statement on the definition
//		public static final String CONSTANT_VALUE1 = "value1";
		
		// Compilation error. Cannot reference an static value before it's defined 
//		VALUE1(CONSTANT_VALUE1),
		VALUE1("value1_name"),
		/* Enums cannot be extended from outside, 
		 * but if its a member this definition acts like an "extends"
		 */
		VALUE2("value2_name") 
		{
			@Override
			public String getTestValueName()
			{
				return testValueName + " OVERRIDE";
			}
		};
		
		// Enums can defined constants that can be referenced from outside. Same for instance and static methods
		public static final String CONSTANT_VALUE1 = "value1_name";
		public static final String CONSTANT_VALUE2 = "value2_name";
		public static final String CONSTANT_STATIC_TEXT = "static text";
		
		// Cannot be private because of VALUE2 override
		protected String testValueName;
		
		// Only private and package private accessors are allowed 
		private ComplexTestEnum(String testValueName)
		{
			this.testValueName = testValueName;
		}
		
		public String getTestValueName()
		{
			return testValueName;
		}
		
		public static String getStaticText()
		{
			return CONSTANT_STATIC_TEXT;
		}
	}
}
