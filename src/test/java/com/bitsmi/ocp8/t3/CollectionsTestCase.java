package com.bitsmi.ocp8.t3;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

import org.junit.jupiter.api.Test;

public class CollectionsTestCase 
{
	@Test
	public void linkedListTest()
	{
		LinkedList<Integer> queue = new LinkedList<>();
		// Add to the end of the queue and return true. Throw an exception if can't
		queue.add(1);
		// Offer add's to the end of the queue. Return false if can't
		queue.offer(2);
		
		// Peek and element retrieves but NOT removes the element at head of the queue
		assertThat(queue.peek()).isEqualTo(1);
		assertThat(queue.element()).isEqualTo(1);
		// Poll and remove retrieves AND removes the element at head of the queue
		assertThat(queue.poll()).isEqualTo(1);
		assertThat(queue.remove()).isEqualTo(2);
		
		// Attempt to retrieve or remove the next element of an empty queue will throw an exception
		assertThat(catchThrowable(() -> {
			queue.element();
		})).isInstanceOf(NoSuchElementException.class);
		assertThat(catchThrowable(() -> {
			queue.remove();
		})).isInstanceOf(NoSuchElementException.class);
		// Peek and pop will return null instead
		assertThat(queue.peek()).isNull();
		assertThat(queue.poll()).isNull();
		
		// A list can insert, get and remove at any position
		queue.add(0, 100);
		queue.get(0);
		queue.remove(0);
		// COMPILATION ERROR: A queue can't
//		((Queue<Integer>)queue).add(0, 100);
//		((Queue<Integer>)queue).get(0);
//		((Queue<Integer>)queue).remove(0);
	}
	
	@Test
	public void queueTest()
	{
		Queue<Integer> queue = new ArrayDeque<>(3);
		
		// Throws IllegalStateException if no space available
		queue.add(1);
		queue.add(2);
		queue.add(3);
		
		assertThat(queue).containsExactly(1, 2, 3);
		
		// Retrieve & remove
		queue.poll();
		// Retrieve but NOT remove
		Integer head = queue.peek();		
		
		assertThat(head).isEqualTo(2);
		assertThat(queue).containsExactly(2, 3);
		
		// Return boolean
		queue.offer(4);
		
		assertThat(queue).containsExactly(2, 3, 4);
	}
	
	@Test
	public void dequeTest()
	{
		Deque<Integer> deque = new ArrayDeque<>();
		
		// Add to final
		deque.add(1);
		deque.add(2);
		deque.addLast(3);
		// Add to head
		deque.addFirst(0);
		
		assertThat(deque).containsExactly(0, 1, 2, 3);
		
		// Retrieve & remove
		deque.poll();		// 0
		deque.pollLast();	// 3
		// Retrieve but NOT remove
		Integer head = deque.peek();
		Integer first = deque.peekFirst();
		Integer tail = deque.peekLast();
		
		assertThat(head).isEqualTo(1);
		assertThat(head).isEqualTo(first);
		assertThat(tail).isEqualTo(2);
		assertThat(deque).containsExactly(1, 2);
		
		deque.offer(4);
		deque.offerLast(5);
		deque.offerFirst(6);
		
		assertThat(deque).containsExactly(6, 1, 2, 4, 5);
	}
	
	@Test
	public void stackTest()
	{
		Deque<Integer> stack = new ArrayDeque<>();
		
		stack.push(1);
		stack.push(2);
		stack.push(3);
		
		assertThat(stack).containsExactly(3, 2, 1);
		
		stack.pop();
		Integer head = stack.peek();
		
		assertThat(head).isEqualTo(2);
		assertThat(stack).containsExactly(2, 1);
	}
	
	@Test
	public void emptyLinkedListTest()
	{
		LinkedList<Integer> list = new LinkedList<>();
		
		assertThat(catchThrowable(() -> {
			// Same for getLast();
			list.getFirst();
		})).isInstanceOf(NoSuchElementException.class);
		
		
		assertThat(catchThrowable(() -> {
			list.element();
		})).isInstanceOf(NoSuchElementException.class);
		
		Integer head = list.peek();
		assertThat(head).isNull();
	}
}
