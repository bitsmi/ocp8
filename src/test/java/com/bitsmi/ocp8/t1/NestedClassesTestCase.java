package com.bitsmi.ocp8.t1;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class NestedClassesTestCase 
{
	@Test
	public void staticNestedClassTest()
	{
		StaticNestedClass testObj = new StaticNestedClass();
		
		assertThat(testObj).isNotNull();
		assertThat(testObj.getStaticValue()).isEqualTo(PRIVATE_ACCESS_TEST_STATIC);
		assertThat(StaticNestedClass.STATIC_CONSTANT).isNotNull();
		assertThat(StaticNestedClass.getStaticConstant()).isNotNull();
	}
	
	@Test
	public void memberInnerClassTest()
	{
		MemberInnerClass testObj = this.new MemberInnerClass();
		
		assertThat(testObj).isNotNull();
		assertThat(testObj.getValue()).isEqualTo(privateAccessTestField);
		// Can access only to static final fields. Non final static fields are not accessible -> COMPILATION ERROR 
		assertThat(MemberInnerClass.STATIC_CONSTANT).isNotNull();
	}
	
	@Test
	public void localInnerClassTest()
	{
		// Static local classes are not allowed -> COMPILATION ERROR
//		static class LocalStaticInnerClass { }
		// But abstract and final are allowed
		abstract class AbstractLocalInnerClass { public abstract String getOuterValue(); }
		
		// Method local classes cannot have access modifier -> COMPILATION ERROR
//		private class LocalInnerClass { }
		class LocalInnerClass extends AbstractLocalInnerClass
		{ 
			// Local inner classes cannot define static fields -> COMPILATION ERROR
//			public static String staticfield = "";
			// But can define static final constants
			private static final String STATIC_CONSTANT = "STATIC";
			
			// Can access to outer class instance fields
			@Override
			public String getOuterValue()
			{
				return privateAccessTestField;
			}
		}
		
		LocalInnerClass testObj = new LocalInnerClass();
		
		assertThat(testObj).isNotNull();
		// Private fields are accessible from within the defining method
		assertThat(LocalInnerClass.STATIC_CONSTANT).isNotNull();
		assertThat(testObj.getOuterValue()).isEqualTo(privateAccessTestField);
	}
	
	@Test 
	public void localInnerClassAccessTest()
	{
		// Compilation error. LocalInnerClass defined in previous method is no accessible outside it's method
//		LocalInnerClass testObj = new LocalInnerClass();
	}
	
	@Test
	public void localAnonymousInnerClassImplementsTest()
	{
		// Local class that implements from StaticNestedInterface
		StaticNestedInterface testObj = new StaticNestedInterface() { };
		
		assertThat(testObj).isNotNull();
		assertThat(testObj).isInstanceOf(StaticNestedInterface.class);
		assertThat(testObj.getClass()).isNotEqualTo(StaticNestedInterface.class);
	}
	
	@Test
	public void localAnonymousInnerClassExtendsTest()
	{
		// Local class that extends from AbstractMemberInnerClass
		AbstractMemberInnerClass testObj = this.new AbstractMemberInnerClass() { };
		// Because the "this" object's class is the outer class, this.new can be omitted
		AbstractMemberInnerClass testObj2 = new AbstractMemberInnerClass() { };
		
		assertThat(testObj).isNotNull();
		assertThat(testObj).isInstanceOf(AbstractMemberInnerClass.class);
		assertThat(testObj.getClass()).isNotEqualTo(AbstractMemberInnerClass.class);
		
		assertThat(testObj2).isNotNull();
	}
	
	@Test
	public void fieldCollisionTest()
	{
		class InnerCollisionClass
		{
			private String collisionField = "inner collision";
			public String getLocalCollisionField() { return collisionField; } 
			public String getOuterCollisionField() { return NestedClassesTestCase.this.collisionField; }
		}
		
		InnerCollisionClass testObj = new InnerCollisionClass();
		
		assertThat(testObj.getLocalCollisionField()).isEqualTo("inner collision");
		assertThat(testObj.getOuterCollisionField()).isEqualTo("outer collision");
	}
	
	/*------------------------------*
	 * SUPPORT METHODS AND CLASSES
	 *------------------------------*/
	// 
	private String privateAccessTestField = "private_field";
	private String collisionField = "outer collision";
	private static final String PRIVATE_ACCESS_TEST_STATIC = "PRIVATE STATIC";
	
	private static class StaticNestedClass 
	{ 
		// Member inner classes can define static fields and constants
		public static String staticfield = "static_field";
		public static final String STATIC_CONSTANT = "STATIC";
		
		// Also, they can define static methods
		public static String getStaticConstant() 
		{ 
			return STATIC_CONSTANT;
		}
		
		public String getStaticValue()
		{
			// Can access to outer class STATIC fields with any access modifier
			String localField = PRIVATE_ACCESS_TEST_STATIC;
			return localField;
		}
	}
	
	private class MemberInnerClass 
	{
		// Member inner classes cannot define static fields -> COMPILATION ERROR
//		public static String staticfield = "";
		// But can define static final constants
		public static final String STATIC_CONSTANT = "STATIC";
		
		// As of static fields, they cannot define static methods -> COMPILATION ERROR
//		public static void getStaticConstant() { }
		
		public String getValue()
		{
			// Can access to outer class fields with any access modifier
			String localField = privateAccessTestField;
			return localField;
		}
	}
	
	/* Member inner classes can have any access modifier (public, private, default, protected)
	 * Can be abstract and final
	 */
	public abstract class AbstractMemberInnerClass { }
	
	private static interface StaticNestedInterface { }
}
