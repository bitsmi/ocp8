package com.bitsmi.ocp8.t3;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.lang.reflect.ParameterizedType;

import org.junit.jupiter.api.Test;

public class GenericSuperclassTestCase 
{
	@Test
	public void getGenericSuperclassTest()
	{
		ConcreteExtendedClass testSimple = new ConcreteExtendedClass("TEST");
		
		assertThat(testSimple.valueClass).isEqualTo(String.class);
	}
	
	@Test
	public void getGenericSuperclassInlineTest()
	{
		// Also works with local classes
		GenericAbstractClass<?> testLocalClass = new GenericAbstractClass<String>("TEST"){ };
		/* But the concrete type must included at the subclass declaration
		 * This code will compile but will raise a exception at runtime
		 */
		Throwable throwableLocalClassWithoutType = catchThrowable(() -> {
			GenericAbstractClass<String> testLocalClassWithoutType = new GenericAbstractClass("TEST"){ };
		});
		
		assertThat(testLocalClass.valueClass).isEqualTo(String.class);
		assertThat(throwableLocalClassWithoutType).isNotNull();
	}
	
	/*------------------------------*
	 * SUPPORT METHODS AND CLASSES
	 *------------------------------*/
	static abstract class GenericAbstractClass<T>
	{
		T value;
		Class<T> valueClass;
		
		public GenericAbstractClass(T value)
		{
			this.value = value;
			/* Extract generic type definition. This only works for subclasses extending this abstract class 
			 * by explicitly defining a concrete type for the generic. E.G.: ConcreteClass extends GenericAbstractClass<String>
			 * In that case, getGenericSuperclass() method will return GenericAbstractClass<String> class definition and it will be possible
			 * to extract the concrete type from it.
			 */
			this.valueClass = (Class<T>)((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		}
	}
	
	static class ConcreteExtendedClass extends GenericAbstractClass<String>
	{
		public ConcreteExtendedClass(String value)
		{
			super(value);
		}
	}
}
