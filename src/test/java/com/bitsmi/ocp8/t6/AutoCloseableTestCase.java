package com.bitsmi.ocp8.t6;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import org.junit.jupiter.api.Test;

public class AutoCloseableTestCase 
{
	@Test
	public void suppressedExceptionTest()
	{
		Throwable throwable = catchThrowable(() -> {
			try(TestResource resource = new TestResource()){
				throw new UncheckedTestException("PRIMARY EXCEPTION");
			}
		});
		
		// Primary exception
		assertThat(throwable).isNotNull().isInstanceOf(UncheckedTestException.class);
		// Suppressed exceptions
		assertThat(throwable.getSuppressed()).isNotEmpty().hasSize(1).allSatisfy(t -> {
			assertThat(t).isInstanceOf(UnsupportedOperationException.class);
		});
	}
	
	@Test
	public void catchOverridenExceptionTest()
	{
		Throwable throwable = catchThrowable(() -> {
			try(TestResource resource = new TestResource()){
				throw new UncheckedTestException("PRIMARY EXCEPTION");
			}
			catch(UncheckedTestException e) {
				throw new IllegalStateException("OVERRIDE");
			}
		});
		
		// Primary exception
		assertThat(throwable).isNotNull().isInstanceOf(IllegalStateException.class);
		// Suppressed exceptions
		assertThat(throwable.getSuppressed()).isEmpty();
	}
	
	@Test
	public void finallyOverridenExceptionTest()
	{
		Throwable throwable = catchThrowable(() -> {
			try(TestResource resource = new TestResource()){
				throw new UncheckedTestException("PRIMARY EXCEPTION");
			}
			finally {
				throw new IllegalStateException("OVERRIDE");
			}
		});
		
		// Primary exception
		assertThat(throwable).isNotNull().isInstanceOf(IllegalStateException.class);
		// Suppressed exceptions
		assertThat(throwable.getSuppressed()).isEmpty();
	}
	
	@Test
	public void closeExceptionTest()
	{
		Throwable throwable = catchThrowable(() -> {
			try(TestResource resource = new TestResource()){
				// Do nothing...
			}			
		});
		
		// Primary exception
		assertThat(throwable).isNotNull().isInstanceOf(UnsupportedOperationException.class);
		// Suppressed exceptions
		assertThat(throwable.getSuppressed()).isEmpty();
	}
	
	/*------------------------------*
	 * SUPPORT METHODS AND CLASSES
	 *------------------------------*/
	private static class TestResource implements AutoCloseable
	{
		@Override
		public void close() throws Exception 
		{
			throw new UnsupportedOperationException("CLOSE EXCEPTION");
		}
	}
	
	private static class UncheckedTestException extends RuntimeException
	{
		private static final long serialVersionUID = 1L;
		
		public UncheckedTestException(String message)
		{
			super(message);
		}
	}
}
