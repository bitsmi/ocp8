package com.bitsmi.ocp8.t2;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class InterfaceDefaultMethodsTestCase 
{
	@Test
	public void polimorphismTest()
	{
		BaseInterface baseObj = new BaseClass();
		ExtendedInterface extendedObj = new ExtendedClass();
		BaseInterface castObj = extendedObj;
		
		assertThat(baseObj.getValue()).isEqualTo("Base");
		assertThat(extendedObj.getValue()).isEqualTo("Extended");
		assertThat(castObj.getValue()).isEqualTo("Extended");
		assertThat(((BaseInterface)extendedObj).getValue()).isEqualTo("Extended");
	}
	
	@Test
	public void overrideTest()
	{
		OverrideClass obj = new OverrideClass();
		
		assertThat(obj.getValue()).isEqualTo("Base Override");
	}
	
	/*------------------------------*
	 * SUPPORT METHODS AND CLASSES
	 *------------------------------*/
	static interface BaseInterface
	{
		public default String getValue()
		{
			return "Base";
		}
	}
	
	static interface ExtendedInterface extends BaseInterface
	{
		@Override
		public default String getValue()
		{
			return "Extended";
		}
	}
	
	static class BaseClass implements BaseInterface
	{
		
	}
	
	static class ExtendedClass implements ExtendedInterface
	{
		
	}
	
	static class OverrideClass implements BaseInterface
	{

		@Override
		public String getValue() 
		{
			// Super call must include interface name
			return BaseInterface.super.getValue() + " Override";
		}
	}
}
