package com.bitsmi.ocp8.t5;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.time.DateTimeException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.time.temporal.UnsupportedTemporalTypeException;

import org.junit.jupiter.api.Test;

public class LocalDateAndTimeTestCase 
{
	@Test
	public void immutabilityTest()
	{
		LocalDate testDate = LocalDate.of(2019, 1, 15);
		LocalDate nextYearDate = testDate.plus(1, ChronoUnit.YEARS);
		
		assertThat(testDate.getYear()).isEqualTo(2019);
		assertThat(nextYearDate.getYear()).isEqualTo(2020);
	}
	
	@Test
	public void manipulationTest()
	{
		LocalDate currentDate = LocalDate.of(2019, 2, 1);
		LocalTime currentTime = LocalTime.of(12, 0);
		// 01/02/2019 12:00
		LocalDateTime currentDateTime = LocalDateTime.of(currentDate, currentTime);
		Period daysPeriod = Period.ofDays(2);
		Duration daysDuration = Duration.ofDays(2);
		Duration hoursDuration = Duration.ofHours(10);
		
		final LocalDate testDate = currentDate.plus(daysPeriod);
		Throwable durationNotAllowedException = catchThrowable(() -> {
			/* LocalDate only works with periods. Durations are no allowed
			 * At this point, it doesn't matter if testDate is not reassigned
			 * as of LocalDate immutability because it will throw an exception
			 */
			testDate.plus(daysDuration);
		});
		
		// 2 days duration will add 48H, so it's valid
		LocalTime testTime = currentTime.plus(daysDuration);
		Throwable periodNotAllowedException = catchThrowable(() -> {
			/* LocalTime only works with durations. Periods are no allowed
			 * At this point, it doesn't matter if testTime is not reassigned
			 * as of LocalTime immutability because it will throw an exception
			 */
			testTime.plus(daysPeriod);
		});
		
		LocalDateTime testDateTime = currentDateTime.plus(daysPeriod);
		testDateTime = testDateTime.plus(daysDuration);
		testDateTime = testDateTime.plus(hoursDuration);
		
		// 1 + 2
		assertThat(testDate.getDayOfMonth()).isEqualTo(3);
		assertThat(testTime.getHour()).isEqualTo(12);
		assertThat(periodNotAllowedException).isInstanceOf(UnsupportedTemporalTypeException.class);
		assertThat(testDateTime.getDayOfMonth()).isEqualTo(5);
		assertThat(durationNotAllowedException).isInstanceOf(UnsupportedTemporalTypeException.class);
		// 12h + 10h
		assertThat(testDateTime.getHour()).isEqualTo(22);
	}
	
	@Test
	public void dateDiffTest()
	{
		LocalDate currentDate = LocalDate.of(2019, 2, 1);
		LocalDateTime currentDateTime = LocalDateTime.now();
		
		LocalDate futureDate = currentDate.plusDays(31);
		/* Only admits difference calculation for LocalDate objects
		 * The resulting period object represents the number of days, months and years passed from currentDate to futureDate
		 * For this example the result will be P1M3D instead of P31D  
		 */
		Period dateDiff = Period.between(currentDate, futureDate);
		Throwable wrongPeriodUnitException = catchThrowable(() -> {
			// Period only admits "get" operations of days, months and years. For all other units, an exception will be thrown
			assertThat(dateDiff.get(ChronoUnit.SECONDS)).isEqualTo(0);
		});
		
		LocalDateTime futureDateTime = currentDateTime.plus(31, ChronoUnit.DAYS)
				.plusHours(12);
		Duration dateTimeDiff = Duration.between(currentDateTime, futureDateTime);
		Throwable wrongDurationUnitException = catchThrowable(() -> {
			// Duration only admits "get" operations of seconds and nanos. For all other units, an exception will be thrown
			assertThat(dateTimeDiff.get(ChronoUnit.DAYS)).isEqualTo(31);
		});
		
		assertThat(dateDiff.getDays()).isEqualTo(3);
		assertThat(dateDiff.getMonths()).isEqualTo(1);
		assertThat(wrongPeriodUnitException).isInstanceOf(DateTimeException.class);
		// 31d*24h*3600s + 12h*3600s = 2_721_600s.
		assertThat(dateTimeDiff.getSeconds()).isEqualTo(2_721_600);
		assertThat(wrongDurationUnitException).isInstanceOf(DateTimeException.class);
	}
}
