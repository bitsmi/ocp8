package com.bitsmi.ocp8.t5;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ListResourceBundle;
import java.util.Locale;
import java.util.ResourceBundle;

import org.junit.jupiter.api.Test;

public class ResourceBundleTestCase 
{
	@Test
	public void javaResourceBundleTest()
	{
		// Nested classes must include parent class and "$" separator
		ResourceBundle bundle = ResourceBundle.getBundle("com.bitsmi.ocp8.t5.ResourceBundleTestCase$TestResourceBundle", new Locale("es", "ES"));
		assertThat(bundle.getString("stringKey")).isEqualTo("valueES");
		assertThat(bundle.getObject("booleanKey")).isEqualTo(Boolean.TRUE);
		assertThat(bundle.getObject("numericKey")).isEqualTo(1);
	}
	
	@Test
	public void propertiesResourceBundleTest()
	{
		// en_EN is in a property file
		ResourceBundle bundle = ResourceBundle.getBundle("com.bitsmi.ocp8.t5.ResourceBundleTestCase$TestResourceBundle", new Locale("en", "EN"));
		// This properties will be loaded from default java class because they aren't in properties file
		assertThat(bundle.getString("stringKey")).isEqualTo("valueEN");
		assertThat(bundle.getObject("booleanKey")).isEqualTo(Boolean.TRUE);
		// Values from properties are always strings
		assertThat(bundle.getObject("numericKey")).isEqualTo("2");
		assertThat(bundle.getObject("numericKey")).isNotEqualTo(2);
	}
	
	public static class TestResourceBundle extends ListResourceBundle
	{
		@Override
		protected Object[][] getContents() 
		{
			// Java class resource bundles allow non String values
			return new Object[][] {				
				{"stringKey", "valueDef"},
				{"booleanKey", Boolean.TRUE},
				{"numericKey", 1}
			};
		}
	}
	
	public static class TestResourceBundle_es_ES extends ListResourceBundle
	{
		@Override
		protected Object[][] getContents() 
		{
			return new Object[][] {
				{"stringKey", "valueES"},
			};
		}
	}
}
