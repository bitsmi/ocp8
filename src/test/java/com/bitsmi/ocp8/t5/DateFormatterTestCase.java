package com.bitsmi.ocp8.t5;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;

public class DateFormatterTestCase 
{
	@Test
	public void localDateParseTest()
	{
		// ISO Date -> YYYY-MM-DD
		Throwable invalidIsoDateThrowable = catchThrowable(() -> {
			LocalDate invalidLocalDate = LocalDate.parse("01/02/2019", DateTimeFormatter.ISO_LOCAL_DATE);
		});
		
		// LocalDate only accept local date formats
		Throwable invalidLocalDateThrowable = catchThrowable(() -> {
			LocalDate invalidLocalDate = LocalDate.parse("2019-02-01", DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		});
		
		LocalDate defaultLocalDate = LocalDate.parse("2019-02-01");
		LocalDate customPatternLocalDate = LocalDate.parse("01/02/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		
		// Date assertions use AssertJ internal String conversion. Check expressions must be always yyyy-MM-dd
		assertThat(invalidIsoDateThrowable).isNotNull().isInstanceOf(DateTimeException.class);
		assertThat(invalidLocalDateThrowable).isNotNull().isInstanceOf(DateTimeException.class);
		assertThat(defaultLocalDate).isNotNull().isEqualTo("2019-02-01");
		assertThat(customPatternLocalDate).isNotNull().isEqualTo("2019-02-01");
	}
	
	@Test
	public void localDateFormatTest()
	{
		// LocalDate only accept local date formats
		Throwable invalidLocalDateThrowable = catchThrowable(() -> {
			String invalidLocalDate = LocalDate.of(2019, 2, 1).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		});
		
		String defaultLocalDate = LocalDate.of(2019, 2, 1).format(DateTimeFormatter.ISO_LOCAL_DATE);
		String customPatternLocalDate = LocalDate.of(2019, 2, 1).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		
		assertThat(invalidLocalDateThrowable).isNotNull().isInstanceOf(DateTimeException.class);
		assertThat(defaultLocalDate).isNotNull().isEqualTo("2019-02-01");
		assertThat(customPatternLocalDate).isNotNull().isEqualTo("01/02/2019");
	}
	
	@Test
	public void zonedDateParseTest()
	{
		/* z -> +01:00, UTC+01:00 format
		 * VV -> Europe/Madrid format
		 * Z -> +0100 format
		 */
		ZonedDateTime defaultDate = ZonedDateTime.parse("2019-02-01T00:11:22+01:00");
		ZonedDateTime isoDate = ZonedDateTime.parse("2019-02-01T00:11:22+01:00", DateTimeFormatter.ISO_DATE_TIME);
		ZonedDateTime customPatternDate = ZonedDateTime.parse("01/02/2019 00:11:22 UTC+01:00", DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss z"));
		ZonedDateTime customPatternDateWithArea = ZonedDateTime.parse("01/02/2019 00:11:22 Europe/Madrid", DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss [VV]"));
		ZonedDateTime customPatternDateNumeric = ZonedDateTime.parse("01/02/2019 00:11:22 +0100", DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss Z"));
		
		// Date assertions use AssertJ internal String conversion. Check expressions must be always yyyy-MM-ddTHH:mm:ssz 
		assertThat(defaultDate).isNotNull().isEqualTo("2019-02-01T00:11:22+01:00");
		assertThat(isoDate).isNotNull().isEqualTo("2019-02-01T00:11:22+01:00");
		assertThat(customPatternDate).isNotNull().isEqualTo("2019-02-01T00:11:22+01:00");
		assertThat(customPatternDateWithArea).isNotNull().isEqualTo("2019-02-01T00:11:22+01:00");
		assertThat(customPatternDateNumeric).isNotNull().isEqualTo("2019-02-01T00:11:22+01:00");
	}
	
	@Test
	public void zonedDateFormatTest()
	{
		ZonedDateTime testDate = ZonedDateTime.of(2019, 2, 1, 0, 11, 22, 0, ZoneId.of("UTC+1"));
		
		String isoDate = testDate.format(DateTimeFormatter.ISO_DATE_TIME);
		String customPatternDate = testDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss z"));
		String customPatternDateWithArea = testDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss [VV]"));
		String customPatternDateNumeric = testDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss Z"));
		
		assertThat(isoDate).isNotNull().isEqualTo("2019-02-01T00:11:22+01:00[UTC+01:00]");
		assertThat(customPatternDate).isNotNull().isEqualTo("01/02/2019 00:11:22 UTC+01:00");
		assertThat(customPatternDateWithArea).isNotNull().isEqualTo("01/02/2019 00:11:22 UTC+01:00");
		assertThat(customPatternDateNumeric).isNotNull().isEqualTo("01/02/2019 00:11:22 +0100");
	}
}
